%module OpenMMSwig

//These includes enable bits of the std library we need.
%include "std_string.i"
%include "std_vector.i"
%include "stl.i"
%include "exception.i"
%include <windows.i>
%include "std_map.i"
%include "arrays_csharp.i"
//This creates a map for strings.
%template(map_string_string) std::map<std::string, std::string>;

//This sets up our GetVectorAsFloatArray output to be a c# array.
%apply float OUTPUT[] {float *output}
%apply float INPUT[] {float *input_positions}
%apply float INPUT[] {float *input_velocities}

//Extensions to get stuff from OpenMM in a way that avoids too much initialisation.
%typemap(cscode) OpenMM::State %{

	float[] positions;
	float[] velocities;
	float[] forces;

    public float[] GetPositionsAsArray()
	{
		if(positions == null)
		{
			positions = new float[3*getNumParticles()];
			GetVectorAsFloatArray(State.DataType.Positions, positions);
		}
		return positions;
	}

	public float[] GetForcesAsArray()
	{
		if(forces == null)
		{
			forces = new float[3*getNumParticles()];
			GetVectorAsFloatArray(State.DataType.Forces, forces);
		}
		return forces;
	}
 
 
    public float[] GetVelocitiesAsArray()
	{
		if(velocities == null)
		{
			velocities = new float[3*getNumParticles()];
			GetVectorAsFloatArray(State.DataType.Positions, velocities);
		}
		return velocities;
	}

%}


%{
    //Include all the OpenMM header files.
    #include <sstream>
    #include <iostream>
    #include "openmm/internal/windowsExport.h"
    #include "openmm/Force.h"
    #include "openmm/Integrator.h"
    #include "openmm/Vec3.h"
    #include "openmm/System.h"
    #include "openmm/State.h"
    #include "openmm/Units.h"
    #include "openmm/Platform.h"
    #include "openmm/TabulatedFunction.h"
    #include "openmm/AndersenThermostat.h"
    #include "openmm/BrownianIntegrator.h"
    #include "openmm/CMAPTorsionForce.h"
    #include "openmm/CMMotionRemover.h"
    #include "openmm/CustomBondForce.h"
    #include "openmm/CustomCompoundBondForce.h"
    #include "openmm/CustomAngleForce.h"
    #include "openmm/CustomTorsionForce.h"
    #include "openmm/CustomExternalForce.h"
    #include "openmm/CustomGBForce.h"
    #include "openmm/CustomHbondForce.h"
    #include "openmm/CustomIntegrator.h"
    #include "openmm/CustomManyParticleForce.h"
    #include "openmm/CustomNonbondedForce.h"	
    #include "openmm/GBSAOBCForce.h"
    #include "openmm/HarmonicAngleForce.h"
    #include "openmm/HarmonicBondForce.h"	
    #include "openmm/LangevinIntegrator.h"
    #include "openmm/LocalEnergyMinimizer.h"
    #include "openmm/MonteCarloAnisotropicBarostat.h"
    #include "openmm/MonteCarloBarostat.h"
    #include "openmm/MonteCarloMembraneBarostat.h"
    #include "openmm/NonbondedForce.h"
    #include "openmm/Context.h"
    #include "openmm/OpenMMException.h"
    #include "openmm/PeriodicTorsionForce.h"
    #include "openmm/RBTorsionForce.h"
    #include "openmm/VariableLangevinIntegrator.h"
    #include "openmm/VariableVerletIntegrator.h"

    #include "openmm/VerletIntegrator.h"
    #include "openmm/VirtualSite.h"
    #include "openmm/serialization/XmlSerializer.h"
%}


%include "openmm/internal/windowsExport.h"
%include "openmm/Force.h"
%include "openmm/Integrator.h"
%include "openmm/TabulatedFunction.h"
%include "openmm/Vec3.h"



//This creates a class representing std::vector<OpenMM::Vec3>
%template(OpenMMVectorVec3) std::vector<OpenMM::Vec3>;

//Terrible hack for now. @todo make a more efficient Vec3.
%extend OpenMM::Vec3 {
  double x() {
      return (*self)[0];
  }
  double y() {
      return (*self)[1];
  }
  double z() {
      return (*self)[2];
  }
}


%ignore ConstraintInfo;
%include "openmm/System.h"
%include "openmm/State.h"
%include "openmm/Units.h"
%include "openmm/Platform.h"
%include "openmm/AndersenThermostat.h"
%include "openmm/BrownianIntegrator.h"
//There are a load of classes and functions that are supposed to be private but declared as public in OpenMM.
%ignore  OpenMM::CMAPTorsionForce::MapInfo;
%ignore  OpenMM::CMAPTorsionForce::CMAPTorsionInfo;
%include "openmm/CMAPTorsionForce.h"
%include "openmm/CMMotionRemover.h"
%ignore OpenMM::CustomBondForce::BondInfo;
%ignore OpenMM::CustomBondForce::BondParameterInfo;
%ignore OpenMM::CustomBondForce::GlobalParameterInfo;
%include "openmm/CustomBondForce.h"
%ignore BondInfo;
%ignore BondParameterInfo;
%ignore GlobalParameterInfo;
%ignore FunctionInfo;
%include "openmm/CustomCompoundBondForce.h"
%ignore AngleInfo;
%ignore AngleParameterInfo;
%include "openmm/CustomAngleForce.h"
%ignore TorsionInfo;
%ignore TorsionParameterInfo;
%include "openmm/CustomTorsionForce.h"
%ignore ParticleInfo;
%ignore PerParticleInfo;
%ignore ParticleParameterInfo;
%ignore PerParticleParameterInfo;
%include "openmm/CustomExternalForce.h"
%ignore ExclusionInfo;
%ignore ComputationInfo;
%ignore GroupInfo;
%ignore PerPairParameterInfo;
%ignore ComputationInfo; 
%include "openmm/CustomGBForce.h"
%include "openmm/CustomHbondForce.h"
%include "openmm/CustomIntegrator.h"
%include "openmm/CustomManyParticleForce.h"
%ignore InteractionGroupInfo; 
%include "openmm/CustomNonbondedForce.h"

%include "openmm/GBSAOBCForce.h"
%include "openmm/HarmonicAngleForce.h"
%include "openmm/HarmonicBondForce.h"

%include "openmm/LangevinIntegrator.h"
%include "openmm/LocalEnergyMinimizer.h"
%include "openmm/MonteCarloAnisotropicBarostat.h"
%include "openmm/MonteCarloBarostat.h"
%include "openmm/MonteCarloMembraneBarostat.h"
%ignore ExceptionInfo;
%include "openmm/NonbondedForce.h"
%include "openmm/Context.h"
%include "openmm/OpenMMException.h"
%ignore PeriodicTorsionInfo;
%include "openmm/PeriodicTorsionForce.h"
%ignore RBTorsionInfo;
%include "openmm/RBTorsionForce.h"
%include "openmm/VariableLangevinIntegrator.h"
%include "openmm/VariableVerletIntegrator.h"

%include "openmm/VerletIntegrator.h"
%include "openmm/VirtualSite.h"
%include "openmm/serialization/XmlSerializer.h"

//Extension for serializing. 
%extend OpenMM::XmlSerializer {
  static std::string serializeSystem(const OpenMM::System* object) {
      std::stringstream ss;
      OpenMM::XmlSerializer::serialize<OpenMM::System>(object, "System", ss);
      return ss.str();
  }

  %newobject deserializeSystem;
  static OpenMM::System* deserializeSystem(const char* inputString) {
      std::stringstream ss;
      ss << inputString;
      return OpenMM::XmlSerializer::deserialize<OpenMM::System>(ss);
  }
  
  static std::string _serializeForce(const OpenMM::Force* object) {
      std::stringstream ss;
      OpenMM::XmlSerializer::serialize<OpenMM::Force>(object, "Force", ss);
      return ss.str();
  }

  %newobject _deserializeForce;
  static OpenMM::Force* _deserializeForce(const char* inputString) {
      std::stringstream ss;
      ss << inputString;
      return OpenMM::XmlSerializer::deserialize<OpenMM::Force>(ss);
  }
  
  static std::string _serializeIntegrator(const OpenMM::Integrator* object) {
      std::stringstream ss;
      OpenMM::XmlSerializer::serialize<OpenMM::Integrator>(object, "Integrator", ss);
      return ss.str();
  }

  %newobject _deserializeIntegrator;
  static OpenMM::Integrator* _deserializeIntegrator(const char* inputString) {
      std::stringstream ss;
      ss << inputString;
      return OpenMM::XmlSerializer::deserialize<OpenMM::Integrator>(ss);
  }
}

%extend OpenMM::State {
  
  int getNumParticles() {
      if ((self->getDataTypes() & OpenMM::State::Positions) != 0)
          return self->getPositions().size();
      if ((self->getDataTypes() & OpenMM::State::Velocities) != 0)
          return self->getVelocities().size();
      if ((self->getDataTypes() & OpenMM::State::Forces) != 0)
          return self->getForces().size();
      return 0;
  }
  

  void GetVectorAsFloatArray(OpenMM::State::DataType type, float* output) {
	const std::vector<OpenMM::Vec3>* array;
	if (type == OpenMM::State::Positions)
		array = &self->getPositions();
	else if (type == OpenMM::State::Velocities)
		array = &self->getVelocities();
	else if (type == OpenMM::State::Forces)
		array = &self->getForces();
	else {
		return;
	}
	std::vector<float> outputArray;
	outputArray.resize(3 * array->size());
	for (int i = 0; i < array->size(); i++)
	{
		outputArray[i * 3 + 0] = (float)((*array)[i][0]);
		outputArray[i * 3 + 1] = (float)((*array)[i][1]);
		outputArray[i * 3 + 2] = (float)((*array)[i][2]);
	}
	std::copy(outputArray.begin(), outputArray.end(), output);
  }
}

%extend OpenMM::Context{
  void SetPositions(float* input_positions, int natoms)
  {
	std::vector<OpenMM::Vec3> positionsArray(natoms);
	for(int i=0; i < natoms; i++)
	{
	  positionsArray[i][0] = (double) input_positions[i * 3 + 0];
	  positionsArray[i][1] = (double) input_positions[i * 3 + 1];
	  positionsArray[i][2] = (double) input_positions[i * 3 + 2];
	}
	self->setPositions(positionsArray);
  
  }

  void SetVelocities(float* input_velocities, int natoms)
  {
	std::vector<OpenMM::Vec3> array(natoms);
	for(int i=0; i < natoms; i++)
	{
	  array[i][0] = (double) input_velocities[i * 3 + 0];
	  array[i][1] = (double) input_velocities[i * 3 + 1];
	  array[i][2] = (double) input_velocities[i * 3 + 2];
	}
	self->setVelocities(array);
  
  }

}




