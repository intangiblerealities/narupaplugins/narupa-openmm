cmake_minimum_required (VERSION 2.6)
SET(CMAKE_INSTALL_PREFIX /usr/lib)
project (OpenMMSwig)
SET(EXECUTABLE_OUTPUT_PATH bin)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -D_GLIBCXX_USE_CXX11_ABI=0")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -D_GLIBCXX_USE_CXX11_ABI=0")

# use, i.e. don't skip the full RPATH for the build tree
SET(CMAKE_SKIP_BUILD_RPATH  FALSE)

# when building, don't use the install RPATH already
# (but later on when installing)
SET(CMAKE_BUILD_WITH_INSTALL_RPATH FALSE)

SET(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}")

# add the automatically determined parts of the RPATH
# which point to directories outside the build tree to the install RPATH
SET(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)

SET(OPENMM_LIB "$ENV{HOME}/anaconda3/lib" CACHE FILEPATH "Path to OpenMM libraries")
SET(OPENMM_INCLUDE "$ENV{HOME}/anaconda3/include" CACHE FILEPATH "Path to OpenMM include directory")

message("In order to use CPU, CUDA or OpenCL platforms, make sure to set the environment variable OPENMM_PLUGIN_DIR=${OPENMM_LIB}/plugins")


# the RPATH to be used when installing, but only if it's not a system directory
LIST(FIND CMAKE_PLATFORM_IMPLICIT_LINK_DIRECTORIES "${CMAKE_INSTALL_PREFIX}" isSystemDir)
IF("${isSystemDir}" STREQUAL "-1")
   SET(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}")
ENDIF("${isSystemDir}" STREQUAL "-1")


link_directories(${OPENMM_LIB})
include_directories(${OPENMM_INCLUDE})
add_library(OpenMMSwig SHARED OpenMMSwig_wrap.cxx)
target_link_libraries(OpenMMSwig OpenMM)
install (TARGETS OpenMMSwig LIBRARY DESTINATION .)
