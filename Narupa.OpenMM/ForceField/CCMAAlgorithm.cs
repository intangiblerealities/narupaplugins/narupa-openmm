//Based on the reference CCMA implementation in OpenMM.

/* Portions copyright (c) 2006-2017 Stanford University and Simbios.
 * Contributors: Peter Eastman, Pande Group
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS, CONTRIBUTORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MathNet.Numerics.LinearAlgebra;
using SlimMath;

namespace Narupa.OpenMM.ForceField
{
    public class CCMAAlgorithm
    {
        private readonly int[,] atomIndices;
        private readonly List<double> distance;
        private readonly double elementCutoff;
        private readonly int numberOfAtoms;
        private readonly double[] _d_ij2;
        private double[] _distanceTolerance;
        private bool _hasInitializedMasses;
        private readonly List<List<Tuple<int, double>>> _matrix;

        private readonly Vector3[] _r_ij;
        private readonly double[] _reducedMasses;

        private double[] constraintDelta;
        private double[] tempDelta;

        private Matrix<double> matrixNet;
        private int numberOfConstraints;
       
        private int[,] CopyAtomIndices(List<Tuple<int, int>> atomIndices)
        {
            var array = new int[atomIndices.Count, 2];
            for (int i = 0; i < atomIndices.Count; i++)
            {
                array[i, 0] = atomIndices[i].Item1;
                array[i, 1] = atomIndices[i].Item2;
            }

            return array;
        }
        
        public CCMAAlgorithm(int numberOfAtoms, int numberOfConstraints, List<Tuple<int, int>> atomIndices,
            List<double> distance, List<float> masses, List<AngleInfo> angles, double elementCutoff)
        {
            this.numberOfAtoms = numberOfAtoms;
            this.numberOfConstraints = numberOfConstraints;
            this.atomIndices = CopyAtomIndices(atomIndices);
            this.distance = distance;
            this.elementCutoff = elementCutoff;

            MaximumNumberOfIterations = 150;

            if (numberOfConstraints > 0)
            {
                _r_ij = new Vector3[numberOfConstraints];
                _d_ij2 = new double[numberOfConstraints];
                _distanceTolerance = new double[numberOfConstraints];
                _reducedMasses = new double[NumberOfConstraints];
            }

            if (numberOfConstraints > 0)
            {
                // Compute the constraint coupling matrix
                var atomAngles = new List<List<int>>(numberOfAtoms);
                for (var i = 0; i < numberOfAtoms; i++) atomAngles.Add(new List<int>());

                for (var i = 0; i < angles.Count; i++)
                    atomAngles[angles[i].B].Add(i);


                var matrix = new List<List<Tuple<int, double>>>(numberOfConstraints);
                matrixNet = Matrix<double>.Build.Dense(numberOfConstraints, numberOfConstraints);
                for (var i = 0; i < numberOfConstraints; i++)
                    matrix.Add(new List<Tuple<int, double>>());

                for (var j = 0; j < numberOfConstraints; j++)
                for (var k = 0; k < numberOfConstraints; k++)
                {
                    if (j == k)
                    {
                        matrix[j].Add(new Tuple<int, double>(j, 1.0));
                        matrixNet[j, j] = 1.0f;
                        continue;
                    }

                    double scale;
                    var atomj0 = atomIndices[j].Item1;
                    var atomj1 = atomIndices[j].Item2;
                    var atomk0 = atomIndices[k].Item1;
                    var atomk1 = atomIndices[k].Item2;
                    double invMass0 = 1 / masses[atomj0];
                    double invMass1 = 1 / masses[atomj1];
                    int atoma, atomb, atomc;
                    if (atomj0 == atomk0)
                    {
                        atoma = atomj1;
                        atomb = atomj0;
                        atomc = atomk1;
                        scale = invMass0 / (invMass0 + invMass1);
                    }
                    else if (atomj1 == atomk1)
                    {
                        atoma = atomj0;
                        atomb = atomj1;
                        atomc = atomk0;
                        scale = invMass1 / (invMass0 + invMass1);
                    }
                    else if (atomj0 == atomk1)
                    {
                        atoma = atomj1;
                        atomb = atomj0;
                        atomc = atomk0;
                        scale = invMass0 / (invMass0 + invMass1);
                    }
                    else if (atomj1 == atomk0)
                    {
                        atoma = atomj0;
                        atomb = atomj1;
                        atomc = atomk1;
                        scale = invMass1 / (invMass0 + invMass1);
                    }
                    else
                    {
                        continue; // These constraints are not connected.
                    }

                    // Look for a third constraint forming a triangle with these two.

                    var foundConstraint = false;
                    for (var other = 0; other < numberOfConstraints; other++)
                        if (atomIndices[other].Item1 == atoma && atomIndices[other].Item2 == atomc ||
                            atomIndices[other].Item1 == atomc && atomIndices[other].Item2 == atoma)
                        {
                            var d1 = distance[j];
                            var d2 = distance[k];
                            var d3 = distance[other];
                            matrix[j].Add(new Tuple<int, double>(k,
                                scale * (d1 * d1 + d2 * d2 - d3 * d3) / (2.0 * d1 * d2)));
                            matrixNet[j, k] = scale * (d1 * d1 + d2 * d2 - d3 * d3) / (2.0 * d1 * d2);
                            foundConstraint = true;
                            break;
                        }

                    if (!foundConstraint)
                    {
                        // We didn't find one, so look for an angle force field term.
                        var angleCandidates = atomAngles[atomb];
                        foreach (var candidate in angleCandidates)
                        {
                            var angle = angles[candidate];
                            if (angle.A == atoma && angle.C == atomc || angle.C == atoma && angle.A == atomc)
                            {
                                matrix[j].Add(new Tuple<int, double>(k, scale * Math.Cos(angle.Angle)));
                                matrixNet[j, k] = scale * Math.Cos(angle.Angle);
                                break;
                            }
                        }
                    }
                }

                _matrix = new List<List<Tuple<int, double>>>(numberOfConstraints);
                var qr = matrixNet.QR();

                var inverse = qr.R.Inverse() * qr.Q.Transpose();

                //construct the resulting inverse matrix in sparse form. 
                //TODO, could probably just use mathnet numerics for all of this.
                for (var i = 0; i < numberOfConstraints; i++)
                {
                    _matrix.Add(new List<Tuple<int, double>>());
                    for (var j = 0; j < numberOfConstraints; j++)
                    {
                        //ignore tiny elements.
                        if (!(inverse[i, j] > elementCutoff)) continue;
                        _matrix[i].Add(new Tuple<int, double>(j, inverse[i, j]));

                    }

                }
                
                constraintDelta = new double[numberOfConstraints];
                tempDelta = new double[numberOfConstraints];

                matrixNet = inverse;
            }
        }

        public int NumberOfConstraints => numberOfConstraints;

        public int MaximumNumberOfIterations { get; }

        public void Apply(List<Vector3> atomCoordinates, List<Vector3> atomCoordinatesP, List<float> inverseMasses,
            float tolerance)
        {
            applyConstraints(atomCoordinates, atomCoordinatesP, inverseMasses, false, tolerance);
        }

        public void ApplyToVelocities(List<Vector3> atomCoordinates, List<Vector3> velocities,
            List<float> inverseMasses, float tolerance)
        {
            applyConstraints(atomCoordinates, velocities, inverseMasses, true, tolerance);
        }

        private void applyConstraints(List<Vector3> atomCoordinates, List<Vector3> atomCoordinatesP,
            List<float> inverseMasses, bool constrainingVelocities, float tolerance)
        {
            // temp arrays
            var r_ij = _r_ij;
            var d_ij2 = _d_ij2;
            var reducedMasses = _reducedMasses;
           

            // calculate reduced masses on 1st pass

            if (!_hasInitializedMasses)
            {
                _hasInitializedMasses = true;
                for (var ii = 0; ii < numberOfConstraints; ii++)
                {
                    var atomI = atomIndices[ii,0];
                    var atomJ = atomIndices[ii,1];
                    reducedMasses[ii] = 0.5 / (inverseMasses[atomI] + inverseMasses[atomJ]);
                }
            }

            // setup: r_ij for each (i,j) constraint
            for (var ii = 0; ii < numberOfConstraints; ii++)
            {
                var atomI = atomIndices[ii, 0];
                var atomJ = atomIndices[ii, 1];
                r_ij[ii] = atomCoordinates[atomI] - atomCoordinates[atomJ];
                d_ij2[ii] = Vector3.Dot(r_ij[ii], r_ij[ii]);
            }

            double lowerTol = 1 - 2 * tolerance + tolerance * tolerance;
            double upperTol = 1 + 2 * tolerance + tolerance * tolerance;

            // main loop

            var iterations = 0;
            var numberConverged = 0;

            Array.Clear(tempDelta, 0, tempDelta.Length);
            Array.Clear(constraintDelta, 0, constraintDelta.Length);
            
            while (iterations < MaximumNumberOfIterations)
            {
                numberConverged = 0;
                for (var ii = 0; ii < numberOfConstraints; ii++)
                {
                    var atomI = atomIndices[ii, 0];
                    var atomJ = atomIndices[ii, 1];
                    var rp_ij = atomCoordinatesP[atomI] - atomCoordinatesP[atomJ];
                    if (constrainingVelocities)
                    {
                        double rrpr = Vector3.Dot(rp_ij, r_ij[ii]);
                        constraintDelta[ii] = -2 * reducedMasses[ii] * rrpr / d_ij2[ii];
                        if (Math.Abs(constraintDelta[ii]) <= tolerance)
                            numberConverged++;
                    }
                    else
                    {
                        double rp2 = Vector3.Dot(rp_ij, rp_ij);
                        var dist2 = distance[ii] * distance[ii];
                        var diff = dist2 - rp2;
                        constraintDelta[ii] = 0;
                        double rrpr = Vector3.Dot(rp_ij, r_ij[ii]);
                        constraintDelta[ii] = reducedMasses[ii] * diff / rrpr;
                        if (rp2 >= lowerTol * dist2 && rp2 <= upperTol * dist2)
                            numberConverged++;
                    }
                }

                if (numberConverged == numberOfConstraints)
                    break;
                iterations++;

                if (matrixNet.RowCount > 0)
                {
                    for (var i = 0; i < numberOfConstraints; i++)
                    {
                        var sum = 0.0;
                        
                        for (int j = 0; j < _matrix[i].Count; j++)
                        {
                            sum += _matrix[i][j].Item2 * constraintDelta[_matrix[i][j].Item1];
                        }
                        
                        tempDelta[i] = sum;
                    }

                    constraintDelta = tempDelta;
                }

                for (var ii = 0; ii < numberOfConstraints; ii++)
                {
                    var atomI = atomIndices[ii, 0];
                    var atomJ = atomIndices[ii, 1];
                    var dr = r_ij[ii] * (float) constraintDelta[ii];
                    atomCoordinatesP[atomI] += dr * inverseMasses[atomI];
                    atomCoordinatesP[atomJ] -= dr * inverseMasses[atomJ];
                }
            }
        }
    }
}