﻿//A constraint solver, based on the openmm reference implementation, under the MIT license:

/* -------------------------------------------------------------------------- *
 *                                   OpenMM                                   *
 * -------------------------------------------------------------------------- *
 * This is part of the OpenMM molecular simulation toolkit originating from   *
 * Simbios, the NIH National Center for Physics-Based Simulation of           *
 * Biological Structures at Stanford, funded under the NIH Roadmap for        *
 * Medical Research, grant U54 GM072970. See https://simtk.org.               *
 *                                                                            *
 * Portions copyright (c) 2013-2015 Stanford University and the Authors.      *
 * Authors: Peter Eastman                                                     *
 * Contributors:                                                              *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    *
 * THE AUTHORS, CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,    *
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR      *
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE  *
 * USE OR OTHER DEALINGS IN THE SOFTWARE.                                     *
 * -------------------------------------------------------------------------- */

using System;
using System.Collections.Generic;
using System.Linq;
using Nano.Science.Simulation;
using Nano.Science.Simulation.ForceField;
using Nano.Science.Simulation.Instantiated;
using OpenMMNET;
using Narupa.MD.ForceField.MM3;
using SlimMath;

namespace Narupa.OpenMM.ForceField
{
    public class ConstraintImplementation
    {
        private SETTLEAlgorithm settle;
        private CCMAAlgorithm ccma;

        private List<Constraint> constraints = new List<Constraint>();

        public bool HasConstraints => constraints.Count() > 1;

        public int NumberOfConstraints => constraints.Count();

        public IEnumerable<Constraint> GetConstraints() => constraints;
        
        public ConstraintImplementation(IAtomicSystem system, List<Constraint> constraints, List<AngleInfo> angleInfos = null)
        {
            int numParticles = system.NumberOfParticles;
            var masses = system.Particles.Mass;

            this.constraints = constraints;
            
            List<int> atom1 = new List<int>();
            List<int> atom2 = new List<int>();
            List<double> distance = new List<double>();
            List<int> constraintCount = new List<int>(numParticles);
            for(int i=0; i < numParticles; i++)
                constraintCount.Add(0);
            foreach (var constraint in constraints)
            {
                if (masses[constraint.A] != 0 || masses[constraint.B] != 0)
                {
                    atom1.Add(constraint.A);
                    atom2.Add(constraint.B);
                    distance.Add(constraint.Distance);
                    constraintCount[constraint.A] += 1;
                    constraintCount[constraint.B] += 1; 
                }
            }

            // Identify clusters of three atoms that can be treated with SETTLE.  First, for every
            // atom that might be part of such a cluster, make a list of the two other atoms it is
            // connected to.
            
            List<Dictionary<int, float>> settleConstraints = new List<Dictionary<int, float>>();
            for (int i = 0; i < numParticles; i++)
                settleConstraints.Add(new Dictionary<int, float>());

            for (int i = 0; i < atom1.Count; i++)
            {
                if (constraintCount[atom1[i]] == 2 && constraintCount[atom2[i]] == 2) {
                    settleConstraints[atom1[i]][atom2[i]] = (float) distance[i];
                    settleConstraints[atom2[i]][atom1[i]] = (float) distance[i];
                }
            }

            // Now removes the ones that don't actually form closed loops of three atoms.
            
            List<int> settleClusters= new List<int>();
            for (int i = 0; i < (int) settleConstraints.Count; i++)
            {
                if (settleConstraints[i].Count == 2)
                {
                    var partners = settleConstraints[i].Keys.ToList();
                    int partner1 = partners[0];
                    int partner2 = partners[1];
                    if (settleConstraints[partner1].Count() != 2 || settleConstraints[partner2].Count() != 2 ||
                        !settleConstraints[partner1].ContainsKey(partner2))
                        settleConstraints[i].Clear();
                    else if (i < partner1 && i < partner2)
                        settleClusters.Add(i);
                }
                else
                    settleConstraints[i].Clear();
            }
            
            // recored the SETTLE clusters. 
            
            List<bool> isSettleAtom = new List<bool>(new bool[numParticles]);

            if (settleClusters.Count > 0)
            {
                List<int> atomA = new List<int>();
                List<int> atomB = new List<int>();
                List<int> atomC = new List<int>();

                List<double> distance1 = new List<double>();
                List<double> distance2 = new List<double>();

                for (int i = 0; i < settleClusters.Count; i++)
                {
                    int p1 = settleClusters[i];
                    var constraintAtoms = settleConstraints[p1].ToList();
                    int p2 = constraintAtoms[0].Key;
                    int p3 = constraintAtoms[1].Key;
                    float dist12 = settleConstraints[p1][p2];
                    float dist13 = settleConstraints[p1][p3];
                    float dist23 = settleConstraints[p2][p3];
                    if (dist12 == dist13)
                    {
                        //p1 is the central atom 
                        atomA.Add(p1);
                        atomB.Add(p2);
                        atomC.Add(p3);
                        distance1.Add(dist12);
                        distance2.Add(dist23);
                    }
                    else if (dist12 == dist23)
                    {
                        //p2 is the central atom. 
                        atomA.Add(p2);
                        atomB.Add(p1);
                        atomC.Add(p3);
                        distance1.Add(dist12);
                        distance2.Add(dist13);
                    }
                    else if (dist13 == dist23)
                    {
                        //p3 is the central atom 
                        atomA.Add(p3);
                        atomB.Add(p1);
                        atomC.Add(p2);
                        distance1.Add(dist13);
                        distance2.Add(dist12);
                    }
                    else
                        //cannae handle this with SETTLE.
                        continue;

                    isSettleAtom[p1] = true;
                    isSettleAtom[p2] = true;
                    isSettleAtom[p3] = true;
                    

                }

                if (atomA.Count > 0)
                {
                    settle = new SETTLEAlgorithm(atomA, atomB, atomC, distance1, distance2, masses);
                }
            }
            
            // All other constraints are handled with CCMA.
            List<int> ccmaConstraints = new List<int>();
            for (int i = 0; i < atom1.Count; i++)
            {
                if(!isSettleAtom[atom1[i]])
                    ccmaConstraints.Add(i);
            }

            int numCCMA = ccmaConstraints.Count;
            if (numCCMA > 0)
            {
                // record particles and distances for ccma. 
                List<Tuple<int, int>> ccmaIndices = new List<Tuple<int, int>>(numCCMA);
                List<double> ccmaDistance = new List<double>(numCCMA);
                    
                for (int i = 0; i < numCCMA; i++)
                {
                    var index = ccmaConstraints[i];
                    ccmaIndices.Add(new Tuple<int, int>(atom1[index], atom2[index]));
                    ccmaDistance.Add(distance[index]);
                }
                
                            
                //Get the angles for CCMA 
                ccma = new CCMAAlgorithm(numParticles, numCCMA, ccmaIndices, ccmaDistance, masses, angleInfos, 0.02);
            }

        }

        public void Apply(List<Vector3> atomCoordinates, List<Vector3> atomCoordinatesP, List<float> inverseMasses, float tolerance)
        {
            ccma?.Apply(atomCoordinates, atomCoordinatesP, inverseMasses, tolerance);
            settle?.Apply(atomCoordinates, atomCoordinatesP, inverseMasses, tolerance);
        }
        
  
        public void ApplyToVelocities(List<Vector3> atomCoordinates, List<Vector3> velocities, List<float> inverseMasses, float tolerance)
        {
            ccma?.ApplyToVelocities(atomCoordinates, velocities, inverseMasses, tolerance);
            settle?.ApplyToVelocities(atomCoordinates, velocities, inverseMasses, tolerance);
        }
    }
}