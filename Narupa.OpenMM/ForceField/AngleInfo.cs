﻿namespace Narupa.OpenMM.ForceField
{
    public class AngleInfo
    {
        public int A;
        public int B;
        public int C;
        public float Angle;

        public AngleInfo(int a, int b, int c, float angle)
        {
            A = a;
            B = b;
            C = c;
            Angle = angle;
        }
    }
}