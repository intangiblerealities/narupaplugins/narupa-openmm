﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nano;
using OpenCL.Net;

namespace Narupa.OpenMM.ForceField
{
    public static class OpenCLManager
    {
        public static void PrintPlatformAndDeviceInformation(IReporter reporter)
        {
            if (reporter == null)
                return;
            ErrorCode error;
            Platform[] platforms = OpenCL.Net.Cl.GetPlatformIDs(out error);
            reporter.PrintDetail("Found {0} platforms", platforms.Count());
            int index = 0;
            foreach (Platform platform in platforms)
            {
                InfoBuffer platformName = Cl.GetPlatformInfo(platform, PlatformInfo.Name, out error);
                Device[] devices = Cl.GetDeviceIDs(platform, DeviceType.All, out error);
                reporter.PrintDetail("Platform {0}: {1} has the following {2} device(s) available:", index, platformName, devices.Count());
                int deviceID = 0;
                foreach (Device device in devices)
                {
                    var name = Cl.GetDeviceInfo(device, DeviceInfo.Name, out error);
                    var type = Cl.GetDeviceInfo(device, DeviceInfo.Type, out error);
                    reporter.PrintDetail("Device {0}: {1} ({2})", deviceID, name, type);
                    deviceID++;
                }
                index++;
            }
        }

        public static void SelectPlatformAndDevice(out int chosenPlatform, out int chosenDevice, string preferredPlatform = null, DeviceType preferredDeviceType = DeviceType.Gpu, IReporter reporter = null)
        {
            ErrorCode error;
            Platform[] platforms = Cl.GetPlatformIDs(out error);

            foreach (Platform platform in platforms)
            {
                var platformName = Cl.GetPlatformInfo(platform, PlatformInfo.Name, out error);
                if (preferredPlatform != null)
                {
                    if (platformName.ToString() == preferredPlatform)
                    {
                    }
                }
            }
            if (preferredPlatform != null)
            {
            }
            chosenPlatform = 0;
            chosenDevice = 0;
        }
    }
}