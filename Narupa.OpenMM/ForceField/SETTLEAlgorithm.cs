﻿//Based on the reference CCMA implementation in OpenMM.

/* Portions copyright (c) 2006-2017 Stanford University and Simbios.
 * Contributors: Peter Eastman, Pande Group
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS, CONTRIBUTORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SlimMath;

namespace Narupa.OpenMM.ForceField
{
    public class SETTLEAlgorithm
    {
        private readonly List<int> atom1;
        private readonly List<int> atom2;
        private readonly List<int> atom3;
        private readonly List<double> distance1;
        private readonly List<double> distance2;
        private readonly List<float> masses;

        public SETTLEAlgorithm(List<int> atom1, List<int> atom2, List<int> atom3, List<double> distance1,
            List<double> distance2, List<float> masses)
        {
            this.atom1 = atom1;
            this.atom2 = atom2;
            this.atom3 = atom3;
            this.distance1 = distance1;
            this.distance2 = distance2;
            this.masses = masses;
        }

        public int NumClusters => atom1.Count;

        public void GetClusterParameters(int index, ref int atom1, ref int atom2, ref int atom3, ref double distance1,
            ref double distance2)
        {
            atom1 = this.atom1[index];
            atom2 = this.atom2[index];
            atom3 = this.atom3[index];
            distance1 = this.distance1[index];
            distance2 = this.distance2[index];
        }

        public void Apply(List<Vector3> atomCoordinates, List<Vector3> atomCoordinatesP, List<float> inverseMasses,
            float tolerance)
        {
            Parallel.For(0, atom1.Count, index =>
            {
                var apos0 = atomCoordinates[atom1[index]];
                var xp0 = atomCoordinatesP[atom1[index]] - apos0;
                var apos1 = atomCoordinates[atom2[index]];
                var xp1 = atomCoordinatesP[atom2[index]] - apos1;
                var apos2 = atomCoordinates[atom3[index]];
                var xp2 = atomCoordinatesP[atom3[index]] - apos2;
                var m0 = masses[atom1[index]];
                var m1 = masses[atom2[index]];
                var m2 = masses[atom3[index]];

                // Apply the SETTLE algorithm.

                var xb0 = apos1[0] - apos0[0];
                var yb0 = apos1[1] - apos0[1];
                var zb0 = apos1[2] - apos0[2];
                var xc0 = apos2[0] - apos0[0];
                var yc0 = apos2[1] - apos0[1];
                var zc0 = apos2[2] - apos0[2];

                var invTotalMass = 1 / (m0 + m1 + m2);
                var xcom = (xp0[0] * m0 + (xb0 + xp1[0]) * m1 + (xc0 + xp2[0]) * m2) * invTotalMass;
                var ycom = (xp0[1] * m0 + (yb0 + xp1[1]) * m1 + (yc0 + xp2[1]) * m2) * invTotalMass;
                var zcom = (xp0[2] * m0 + (zb0 + xp1[2]) * m1 + (zc0 + xp2[2]) * m2) * invTotalMass;

                var xa1 = xp0[0] - xcom;
                var ya1 = xp0[1] - ycom;
                var za1 = xp0[2] - zcom;
                var xb1 = xb0 + xp1[0] - xcom;
                var yb1 = yb0 + xp1[1] - ycom;
                var zb1 = zb0 + xp1[2] - zcom;
                var xc1 = xc0 + xp2[0] - xcom;
                var yc1 = yc0 + xp2[1] - ycom;
                var zc1 = zc0 + xp2[2] - zcom;

                var xaksZd = yb0 * zc0 - zb0 * yc0;
                var yaksZd = zb0 * xc0 - xb0 * zc0;
                var zaksZd = xb0 * yc0 - yb0 * xc0;
                var xaksXd = ya1 * zaksZd - za1 * yaksZd;
                var yaksXd = za1 * xaksZd - xa1 * zaksZd;
                var zaksXd = xa1 * yaksZd - ya1 * xaksZd;
                var xaksYd = yaksZd * zaksXd - zaksZd * yaksXd;
                var yaksYd = zaksZd * xaksXd - xaksZd * zaksXd;
                var zaksYd = xaksZd * yaksXd - yaksZd * xaksXd;

                var axlng = Math.Sqrt(xaksXd * xaksXd + yaksXd * yaksXd + zaksXd * zaksXd);
                var aylng = Math.Sqrt(xaksYd * xaksYd + yaksYd * yaksYd + zaksYd * zaksYd);
                var azlng = Math.Sqrt(xaksZd * xaksZd + yaksZd * yaksZd + zaksZd * zaksZd);
                var trns11 = xaksXd / axlng;
                var trns21 = yaksXd / axlng;
                var trns31 = zaksXd / axlng;
                var trns12 = xaksYd / aylng;
                var trns22 = yaksYd / aylng;
                var trns32 = zaksYd / aylng;
                var trns13 = xaksZd / azlng;
                var trns23 = yaksZd / azlng;
                var trns33 = zaksZd / azlng;

                var xb0d = trns11 * xb0 + trns21 * yb0 + trns31 * zb0;
                var yb0d = trns12 * xb0 + trns22 * yb0 + trns32 * zb0;
                var xc0d = trns11 * xc0 + trns21 * yc0 + trns31 * zc0;
                var yc0d = trns12 * xc0 + trns22 * yc0 + trns32 * zc0;
                var za1d = trns13 * xa1 + trns23 * ya1 + trns33 * za1;
                var xb1d = trns11 * xb1 + trns21 * yb1 + trns31 * zb1;
                var yb1d = trns12 * xb1 + trns22 * yb1 + trns32 * zb1;
                var zb1d = trns13 * xb1 + trns23 * yb1 + trns33 * zb1;
                var xc1d = trns11 * xc1 + trns21 * yc1 + trns31 * zc1;
                var yc1d = trns12 * xc1 + trns22 * yc1 + trns32 * zc1;
                var zc1d = trns13 * xc1 + trns23 * yc1 + trns33 * zc1;

                //                                        --- Step2  A2' ---

                var rc = 0.5 * distance2[index];
                var rb = Math.Sqrt(distance1[index] * distance1[index] - rc * rc);
                var ra = rb * (m1 + m2) * invTotalMass;
                rb -= ra;
                var sinphi = za1d / ra;
                var cosphi = Math.Sqrt(1 - sinphi * sinphi);
                var sinpsi = (zb1d - zc1d) / (2 * rc * cosphi);
                var cospsi = Math.Sqrt(1 - sinpsi * sinpsi);

                var ya2d = ra * cosphi;
                var xb2d = -rc * cospsi;
                var yb2d = -rb * cosphi - rc * sinpsi * sinphi;
                var yc2d = -rb * cosphi + rc * sinpsi * sinphi;
                var xb2d2 = xb2d * xb2d;
                var hh2 = 4.0f * xb2d2 + (yb2d - yc2d) * (yb2d - yc2d) + (zb1d - zc1d) * (zb1d - zc1d);
                var deltx = 2.0f * xb2d + Math.Sqrt(4.0f * xb2d2 - hh2 + distance2[index] * distance2[index]);
                xb2d -= deltx * 0.5;

                //                                        --- Step3  al,be,ga ---

                var alpha = xb2d * (xb0d - xc0d) + yb0d * yb2d + yc0d * yc2d;
                var beta = xb2d * (yc0d - yb0d) + xb0d * yb2d + xc0d * yc2d;
                var gamma = xb0d * yb1d - xb1d * yb0d + xc0d * yc1d - xc1d * yc0d;

                var al2be2 = alpha * alpha + beta * beta;
                var sintheta = (alpha * gamma - beta * Math.Sqrt(al2be2 - gamma * gamma)) / al2be2;

                //                                        --- Step4  A3' ---

                var costheta = Math.Sqrt(1 - sintheta * sintheta);
                var xa3d = -ya2d * sintheta;
                var ya3d = ya2d * costheta;
                var za3d = za1d;
                var xb3d = xb2d * costheta - yb2d * sintheta;
                var yb3d = xb2d * sintheta + yb2d * costheta;
                var zb3d = zb1d;
                var xc3d = -xb2d * costheta - yc2d * sintheta;
                var yc3d = -xb2d * sintheta + yc2d * costheta;
                var zc3d = zc1d;

                //                                        --- Step5  A3 ---

                var xa3 = trns11 * xa3d + trns12 * ya3d + trns13 * za3d;
                var ya3 = trns21 * xa3d + trns22 * ya3d + trns23 * za3d;
                var za3 = trns31 * xa3d + trns32 * ya3d + trns33 * za3d;
                var xb3 = trns11 * xb3d + trns12 * yb3d + trns13 * zb3d;
                var yb3 = trns21 * xb3d + trns22 * yb3d + trns23 * zb3d;
                var zb3 = trns31 * xb3d + trns32 * yb3d + trns33 * zb3d;
                var xc3 = trns11 * xc3d + trns12 * yc3d + trns13 * zc3d;
                var yc3 = trns21 * xc3d + trns22 * yc3d + trns23 * zc3d;
                var zc3 = trns31 * xc3d + trns32 * yc3d + trns33 * zc3d;

                xp0[0] = (float) (xcom + xa3);
                xp0[1] = (float) (ycom + ya3);
                xp0[2] = (float) (zcom + za3);
                xp1[0] = (float) (xcom + xb3 - xb0);
                xp1[1] = (float) (ycom + yb3 - yb0);
                xp1[2] = (float) (zcom + zb3 - zb0);
                xp2[0] = (float) (xcom + xc3 - xc0);
                xp2[1] = (float) (ycom + yc3 - yc0);
                xp2[2] = (float) (zcom + zc3 - zc0);

                // Record the new positions.

                atomCoordinatesP[atom1[index]] = xp0 + apos0;
                atomCoordinatesP[atom2[index]] = xp1 + apos1;
                atomCoordinatesP[atom3[index]] = xp2 + apos2;
            });

        }
        
        public void ApplyToVelocities(List<Vector3> atomCoordinates, List<Vector3> velocities, List<float> inverseMasses,
            float tolerance)
        {
             for (int index = 0; index < (int) atom1.Count; ++index) {
        var apos0 = atomCoordinates[atom1[index]];
        var apos1 = atomCoordinates[atom2[index]];
        var apos2 = atomCoordinates[atom3[index]];
        var v0 = velocities[atom1[index]];
        var v1 = velocities[atom2[index]];
        var v2 = velocities[atom3[index]];
        
        // Compute intermediate quantities: the atom masses, the bond directions, the relative velocities,
        // and the angle cosines and sines.
        
        double mA = masses[atom1[index]];
        double mB = masses[atom2[index]];
        double mC = masses[atom3[index]];
        var eAB = apos1-apos0;
        var eBC = apos2-apos1;
        var eCA = apos0-apos2;
        eAB /= (float) Math.Sqrt(eAB[0]*eAB[0] + eAB[1]*eAB[1] + eAB[2]*eAB[2]);
        eBC /= (float) Math.Sqrt(eBC[0]*eBC[0] + eBC[1]*eBC[1] + eBC[2]*eBC[2]);
        eCA /= (float) Math.Sqrt(eCA[0]*eCA[0] + eCA[1]*eCA[1] + eCA[2]*eCA[2]);
        double vAB = (v1[0]-v0[0])*eAB[0] + (v1[1]-v0[1])*eAB[1] + (v1[2]-v0[2])*eAB[2];
        double vBC = (v2[0]-v1[0])*eBC[0] + (v2[1]-v1[1])*eBC[1] + (v2[2]-v1[2])*eBC[2];
        double vCA = (v0[0]-v2[0])*eCA[0] + (v0[1]-v2[1])*eCA[1] + (v0[2]-v2[2])*eCA[2];
        double cA = -(eAB[0]*eCA[0] + eAB[1]*eCA[1] + eAB[2]*eCA[2]);
        double cB = -(eAB[0]*eBC[0] + eAB[1]*eBC[1] + eAB[2]*eBC[2]);
        double cC = -(eBC[0]*eCA[0] + eBC[1]*eCA[1] + eBC[2]*eCA[2]);
        double s2A = 1-cA*cA;
        double s2B = 1-cB*cB;
        double s2C = 1-cC*cC;
        
        // Solve the equations.  These are different from those in the SETTLE paper (JCC 13(8), pp. 952-962, 1992), because
        // in going from equations B1 to B2, they make the assumption that mB=mC (but don't bother to mention they're
        // making that assumption).  We allow all three atoms to have different masses.
        
        double mABCinv = 1/(mA*mB*mC);
        double denom = (((s2A*mB+s2B*mA)*mC+(s2A*mB*mB+2*(cA*cB*cC+1)*mA*mB+s2B*mA*mA))*mC+s2C*mA*mB*(mA+mB))*mABCinv;
        double tab = ((cB*cC*mA-cA*mB-cA*mC)*vCA + (cA*cC*mB-cB*mC-cB*mA)*vBC + (s2C*mA*mA*mB*mB*mABCinv+(mA+mB+mC))*vAB)/denom;
        double tbc = ((cA*cB*mC-cC*mB-cC*mA)*vCA + (s2A*mB*mB*mC*mC*mABCinv+(mA+mB+mC))*vBC + (cA*cC*mB-cB*mA-cB*mC)*vAB)/denom;
        double tca = ((s2B*mA*mA*mC*mC*mABCinv+(mA+mB+mC))*vCA + (cA*cB*mC-cC*mB-cC*mA)*vBC + (cB*cC*mA-cA*mB-cA*mC)*vAB)/denom;
        v0 += (eAB*(float) tab - eCA*(float) tca)*inverseMasses[atom1[index]];
        v1 += (eBC*(float) tbc - eAB*(float) tab)*inverseMasses[atom2[index]];
        v2 += (eCA*(float) tca - eBC*(float) tbc)*inverseMasses[atom3[index]];
        velocities[atom1[index]] = v0;
        velocities[atom2[index]] = v1;
        velocities[atom3[index]] = v2;
    } 
        }
    }
}