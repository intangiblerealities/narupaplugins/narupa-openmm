﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Nano.Science.Simulation;
using Nano.Science.Simulation.Instantiated;
using OpenMMNET;
using System.Collections.Generic;
using Nano;

namespace Narupa.OpenMM.Load.Terms
{
    internal class HarmonicBondForceTerm : IOpenMMForceFieldTerm
    {
        public int ForceGroup;
        public readonly List<BondForceTerm> Bonds = new List<BondForceTerm>();

        public Force GenerateOpenMMForce(InstantiatedTopology topology, SystemProperties properties, IReporter reporter)
        {
            OpenMMNET.HarmonicBondForce force = new OpenMMNET.HarmonicBondForce();
            foreach (BondForceTerm bond in Bonds)
            {
                force.addBond(bond.Atom1, bond.Atom2, bond.EquilibriumDistance, bond.ForceConstant);
            }
            reporter.PrintDetail("Added OpenMM Harmonic Bond Force with {0} terms", force.getNumBonds());
            return force;
        }
    }

    internal class BondForceTerm
    {
        public float EquilibriumDistance;
        public float ForceConstant;
        public int Atom1;
        public int Atom2;

        public BondForceTerm(int atom1, int atom2, float d, float k)
        {
            Atom1 = atom1;
            Atom2 = atom2;
            EquilibriumDistance = d;
            ForceConstant = k;
        }
    }
}