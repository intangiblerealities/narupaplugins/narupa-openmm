﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Nano;
using Nano.Science.Simulation;
using Nano.Science.Simulation.Instantiated;
using OpenMMNET;

namespace Narupa.OpenMM.Load.Terms
{
    internal interface IOpenMMForceFieldTerm
    {
        Force GenerateOpenMMForce(InstantiatedTopology parentTopology, SystemProperties properties, IReporter reporter);
    }
}