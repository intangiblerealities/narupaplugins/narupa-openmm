﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Nano.Science.Simulation;
using Nano.Science.Simulation.Instantiated;
using OpenMMNET;
using System.Collections.Generic;
using Nano;

namespace Narupa.OpenMM.Load.Terms
{
    internal class HarmonicAngleForceTerm : IOpenMMForceFieldTerm
    {
        public int ForceGroup;
        public readonly List<AngleForceTerm> Angles = new List<AngleForceTerm>();

        public Force GenerateOpenMMForce(InstantiatedTopology topology, SystemProperties properties, IReporter reporter)
        {
            HarmonicAngleForce force = new OpenMMNET.HarmonicAngleForce();
            foreach (AngleForceTerm angle in Angles)
            {
                force.addAngle(angle.Atom1, angle.Atom2, angle.Atom3, angle.Angle, angle.K);
            }
            force.setForceGroup(ForceGroup);
            reporter.PrintDetail("Added OpenMM Harmonic Angle Force with {0} terms", force.getNumAngles());
            return force;
        }
    }

    internal class AngleForceTerm
    {
        public float Angle;
        public float K;
        public int Atom1;
        public int Atom2;
        public int Atom3;

        public AngleForceTerm(int atom1, int atom2, int atom3, float angle, float k)
        {
            Atom1 = atom1;
            Atom2 = atom2;
            Atom3 = atom3;
            Angle = angle;
            K = k;
        }
    }
}