﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using OpenMMNET;
using Narupa.OpenMM.Load.ResidueTerms;
using System;
using System.Collections.Generic;
using Nano;
using Nano.Science.Simulation;
using Nano.Science.Simulation.Instantiated;

namespace Narupa.OpenMM.Load.Terms
{
    internal class NonBondedForceTerm : IOpenMMForceFieldTerm
    {
        public readonly List<NonBondedParticle> Particles = new List<NonBondedParticle>();
        public readonly List<ExceptionTerm> Exceptions = new List<ExceptionTerm>();
        internal NonBondedOptions Options;

        public bool GenerateExceptions { get; internal set; }

        //TODO add these scale factors to xml file.
        private float coulomb14scale = 0.833333f;

        private float lj14scale = 0.5f;

        public Force GenerateOpenMMForce(InstantiatedTopology topology, SystemProperties properties, IReporter reporter)
        {
            OpenMMNET.NonbondedForce force = new NonbondedForce();
            force.setForceGroup(Options.ForceGroup);

            foreach (NonBondedParticle particle in Particles)
            {
                force.addParticle(particle.Charge, particle.Sigma, particle.Epsilon);
            }
            reporter.PrintDetail("Added {0} OpenMM Nonbonded Force terms.", Particles.Count);
            if (GenerateExceptions == true)
            {
                if (Exceptions.Count > 0)
                {
                    reporter.PrintWarning(ReportVerbosity.Normal, "GenerateExceptions option specified, but there are already non-bonded Exceptions in the topology. These will be replaced.");
                }
                Exceptions.Clear();
                Exceptions.AddRange(GenerateNonBondedExceptions(topology.Molecules));
            }
            foreach (ExceptionTerm exception in Exceptions)
            {
                try
                {
                    force.addException(exception.Particle1, exception.Particle2, exception.ChargeProduct, exception.Sigma, exception.Epsilon);
                }
                catch (Exception e)
                {
                    reporter.PrintException(e, string.Format("OpenMM threw an exception. Failed to add nonbonded exception between particles {0} and {1} ", exception.Particle1, exception.Particle2));
                }
            }
            reporter.PrintDetail("Added {0} Exceptions to Nonbonded Force terms.", Exceptions.Count);

            force.setNonbondedMethod((NonbondedForce.NonbondedMethod)Options.Method);

            force.setCutoffDistance(Options.CutOff);
            force.setEwaldErrorTolerance(Options.EwaldTolerance);

            NonbondedForce.NonbondedMethod method = (NonbondedForce.NonbondedMethod)Options.Method;
            if (method == NonbondedForce.NonbondedMethod.CutoffNonPeriodic || method == NonbondedForce.NonbondedMethod.NoCutoff)
            {
                if (properties.SimBoundary.PeriodicBoundary)
                {
					reporter.PrintWarning(ReportVerbosity.Normal, "Warning: OpenMM Nonbonded Force set to use non periodic cutoff when periodic boundary specified in Narupa. OpenMM will use a periodic boundary (CutoffPeriodic)");
					method = NonbondedForce.NonbondedMethod.CutoffPeriodic;
                }
            }
            else
            {
                if (properties.SimBoundary.PeriodicBoundary == false)
                {
                    reporter.PrintWarning(ReportVerbosity.Normal, "Warning: OpenMM Nonbonded Force set to use periodic boundary conditions when the simulation is not using periodic boundaries in the integration. OpenMM will not use a periodic boundary (CutoffNonPeriodic");
					method = NonbondedForce.NonbondedMethod.CutoffNonPeriodic;
                }
            }

            force.setPMEParameters(Options.Alpha, Options.Nx, Options.Ny, Options.Nz);
            force.setReactionFieldDielectric(Options.RFDielectric);
            force.setReciprocalSpaceForceGroup(Options.RecipForceGroup);
            force.setSwitchingDistance(Options.SwitchingDistance);
            force.setUseDispersionCorrection(Options.DispersionCorrection);
            force.setUseSwitchingFunction(Options.UseSwitchingFunction);

            return force;
        }

        /// <summary>
        /// Generates the exceptions to LJ for the given set of molecules.
        /// </summary>
        /// <param name="molecules">The molecules.</param>
        /// <remarks>TODO it would be sensible to use OpenMM's generate method rather than rolling our own. </remarks>
        private List<ExceptionTerm> GenerateNonBondedExceptions(List<Molecule> molecules)
        {
            List<ExceptionTerm> exceptions = new List<ExceptionTerm>();
            foreach (Molecule mol in molecules)
            {
                HashSet<BondPair> molExceptions = mol.GetFirstAndSecondNeighbours();
                foreach (BondPair bond in molExceptions)
                {
                    exceptions.Add(new ExceptionTerm(bond.A, bond.B, 0f, 0f, 0f));
                }
                HashSet<BondPair> one4Exceptions = mol.GetThirdNeighbours();
                foreach (BondPair bond in one4Exceptions)
                {
                    float sigma = 0.5f * (Particles[bond.A].Sigma + Particles[bond.B].Sigma);
                    float epsilon = lj14scale * (float)Math.Sqrt(Particles[bond.A].Epsilon * Particles[bond.B].Epsilon);
                    float chargeProduct = coulomb14scale * Particles[bond.A].Charge * Particles[bond.B].Charge;
                    exceptions.Add(new ExceptionTerm(bond.A, bond.B, epsilon, sigma, chargeProduct));
                }
            }
            return exceptions;
        }
    }

    internal class ExceptionTerm
    {
        public int Particle1;
        public int Particle2;
        public float Epsilon;
        public float Sigma;
        public float ChargeProduct;

        public ExceptionTerm(int particle1, int particle2, float epsilon, float sigma, float charge)
        {
            Particle1 = particle1;
            Particle2 = particle2;
            Epsilon = epsilon;
            Sigma = sigma;
            ChargeProduct = charge;
        }
    }
}