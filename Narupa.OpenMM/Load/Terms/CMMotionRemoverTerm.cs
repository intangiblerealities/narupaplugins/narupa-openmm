﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Nano;
using Nano.Science.Simulation;
using Nano.Science.Simulation.Instantiated;
using OpenMMNET;

namespace Narupa.OpenMM.Load.Terms
{
    internal class CMMotionRemoverTerm : IOpenMMForceFieldTerm
    {
        public int ForceGroup;
        public int Frequency;

        public Force GenerateOpenMMForce(InstantiatedTopology topology, SystemProperties properties, IReporter reporter)
        {
            CMMotionRemover force = new CMMotionRemover(Frequency);
            force.setForceGroup(ForceGroup);
            return force;
        }
    }
}