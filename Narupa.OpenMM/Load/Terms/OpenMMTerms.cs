﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Nano.Science.Simulation;
using Nano.Science.Simulation.ForceField;
using Nano.Science.Simulation.Instantiated;
using Narupa.OpenMM.ForceField;
using Narupa.OpenMM.Load.Terms;
using System.Collections;
using System.Collections.Generic;
using Nano;
using Nano.Science;
using Narupa.MD;
using Narupa.MD.System;

namespace Narupa.OpenMM.Load
{
    internal class OpenMMTerms : IInstantiatedForceFieldTerms
    {
        public OpenMMForceField.AvailablePlatforms TargetPlatform = OpenMMForceField.AvailablePlatforms.Reference;

        public readonly List<IOpenMMForceFieldTerm> Forces = new List<IOpenMMForceFieldTerm>();

        public List<Constraint> Constraints = new List<Constraint>();

        public Dictionary<string, string> PlatformProperties = new Dictionary<string, string>();

        public OpenMMTerms()
        {
        }

        public IForceField GenerateForceField(InstantiatedTopology parentTopology, SystemProperties properties, IReporter reporter)
        {
            //OpenMMNET.System system = new OpenMMNET.System();
            reporter.PrintNormal("Initialising OpenMM ForceField.");
            OpenMMForceField f = new OpenMMForceField((SimulationBoundary)properties.SimBoundary, reporter);

            HarmonicAngleForceTerm angleForceTerm = null;
            foreach (IOpenMMForceFieldTerm force in Forces)
            {
                if (force is HarmonicAngleForceTerm)
                    angleForceTerm = (HarmonicAngleForceTerm) force;
                f.AddForce(force.GenerateOpenMMForce(parentTopology, properties, reporter));
                
            }

            
            foreach (Constraint constraint in Constraints)
            {
                f.AddConstraint(constraint);
            }

            //generate list of angles for use with constraints. 
            
            List<AngleInfo> angleInfos = null;
            if (angleForceTerm != null)
            {
                angleInfos = new List<AngleInfo>(angleForceTerm.Angles.Count);
                foreach (var angle in angleForceTerm.Angles)
                {
                    angleInfos.Add(new AngleInfo(angle.Atom1, angle.Atom2, angle.Atom3, angle.Angle));
                }
                f.AddAngleConstraintInfo(angleInfos);
            }
                

            reporter.PrintDetail("Added {0} distance constraints.", Constraints.Count);

            for (int i = 0; i < parentTopology.NumberOfParticles; i++)
            {
                f.AddParticle(PeriodicTable.GetElementProperties(parentTopology.Elements[i]).AtomicWeight);
            }

            f.CreateContext(TargetPlatform, PlatformProperties);
            f.CreateParticleVectors(parentTopology.NumberOfParticles);

            if (properties.SimBoundary.PeriodicBoundary)
            {
                f.SetPeriodicBox(properties.SimBoundary.GetPeriodicBoxVectors());
            }
            reporter.PrintEmphasized("Successfully initialised OpenMM forcefield.");
            return f;
        }
    }
}