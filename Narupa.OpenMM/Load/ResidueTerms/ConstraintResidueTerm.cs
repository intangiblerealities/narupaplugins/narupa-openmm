﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Xml;
using Nano.Science.Simulation.Residues;
using Nano.Loading;
using Nano;

namespace Narupa.OpenMM.Load.ResidueTerms
{
    /// <summary>
    /// Represents an OpenMM constraint.
    /// </summary>
    [XmlName("Constraint")]
    internal class ConstraintResidueTerm : ILoadable
    {
        /// <summary>
        /// The equilibrium distance.
        /// </summary>
        public float D;

        /// <summary>
        /// The path of the first atom.
        /// </summary>
        public AtomPath P1;

        /// <summary>
        /// The path of the second atom.
        /// </summary>
        public AtomPath P2;

        /// <summary>
        /// Loads the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="node">The node.</param>
        public void Load(LoadContext context, XmlNode node)
        {
            D = Helper.GetAttributeValue(node, "d", D);
            P1 = new AtomPath(Helper.GetAttributeValue(node, "p1", string.Empty));
            P2 = new AtomPath(Helper.GetAttributeValue(node, "p2", string.Empty));
        }

        /// <summary>
        /// Saves the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The element.</param>
        /// <returns>XmlElement.</returns>
        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "d", D);
            Helper.AppendAttributeAndValue(element, "p1", P1.ToString());
            Helper.AppendAttributeAndValue(element, "p2", P2.ToString());
            return element;
        }
    }
}