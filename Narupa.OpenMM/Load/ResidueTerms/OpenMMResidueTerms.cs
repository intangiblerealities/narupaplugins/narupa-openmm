﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Nano.Science.Simulation.ForceField;
using Nano.Science.Simulation.Instantiated;
using Nano.Science.Simulation.Residues;
using Nano.Science.Simulation.Spawning;
using Narupa.OpenMM.ForceField;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Xml;
using Nano;
using Nano.Loading;
using SlimMath;

namespace Narupa.OpenMM.Load.ResidueTerms
{
    [XmlName("PlatformProperties")]
    internal class OpenMMPlatformProperties : Dictionary<string, string>, ILoadable
    {
        public void Load(LoadContext context, XmlNode node)
        {
            XmlAttributeCollection atributos = node.Attributes;
            foreach (XmlAttribute at in atributos)
            {
                Add(at.Name, at.Value);
            }
        }

        public XmlElement Save(LoadContext context, XmlElement element)
        {
            foreach (KeyValuePair<string, string> attribute in this)
            {
                Helper.AppendAttributeAndValue(element, attribute.Key, attribute.Value);
            }
            return element;
        }
    }

    [XmlName("OpenMMForceField")]
    internal class OpenMMResidueTerms : IForceFieldTerms, IConstraintTerm
    {
        private OpenMMForceField.AvailablePlatforms desiredPlatform = OpenMMForceField.AvailablePlatforms.CUDA;
        private OpenMMPlatformProperties platformProperties;
        private List<IOpenMMResidueTerm> Forces = new List<IOpenMMResidueTerm>();
        private List<ConstraintResidueTerm> Constraints = new List<ConstraintResidueTerm>();

        private static bool InitialisedOpenMMLibary = false;
        private string pluginDir = "";

        public OpenMMResidueTerms()
        {
        }

        public void AddToTopology(InstantiatedTopology parentTopology, IResidue residue, ref SpawnContext context)
        {
            OpenMMTerms openMMTerms = Utils.GetInstanceOf<OpenMMTerms>(parentTopology.ForceFieldTerms);
            if (openMMTerms == null)
            {
                openMMTerms = new OpenMMTerms();
                parentTopology.ForceFieldTerms.Add(openMMTerms);
            }

            if (openMMTerms.TargetPlatform <= desiredPlatform)
            {
                openMMTerms.TargetPlatform = desiredPlatform;
                openMMTerms.PlatformProperties = platformProperties;
            }

            foreach (IOpenMMResidueTerm force in Forces)
            {
                force.AddToOpenMMTerms(openMMTerms, parentTopology, residue, ref context);
                if (force is NonBondedForceResidueTerm)
                {
                    NonBondedForceResidueTerm nonBonded = force as NonBondedForceResidueTerm;
                }
            }
            foreach (ConstraintResidueTerm constraint in Constraints)
            {
                int atom1 = context.ResolveIndexOfAtom(parentTopology, constraint.P1);
                int atom2 = context.ResolveIndexOfAtom(parentTopology, constraint.P2);
                openMMTerms.Constraints.Add(new Constraint(atom1, atom2, constraint.D));
            }
        }

        public void Load(LoadContext context, XmlNode node)
        {
            pluginDir = Helper.GetAttributeValue(node, "PluginDir", pluginDir);
            if (InitialisedOpenMMLibary == false)
            {
                try
                {
                    if(pluginDir == string.Empty)
                        OpenMMForceField.LoadOpenMMLibraryAndPlugins();
                    else
                        OpenMMForceField.LoadOpenMMLibraryAndPlugins(false, pluginDir);
                }
                catch
                {
                    string error = "Exception thrown attempting to load the OpenMM libary and plugins. Check build configuration.";
                    context.Error(error, true);
                    throw;
                }
                InitialisedOpenMMLibary = true;
            }
            desiredPlatform = Helper.GetAttributeValue(node, "TargetPlatform", desiredPlatform);
            platformProperties = Loader.LoadObject<OpenMMPlatformProperties>(context, node);
            var systemXmlFile = Helper.GetAttributeValue(node, "SystemFile", string.Empty);
            if (systemXmlFile != string.Empty)
                LoadFromOpenMMXmlFile(systemXmlFile, context);
            Constraints.AddRange(Loader.LoadObjects<ConstraintResidueTerm>(context, node.SelectSingleNode("Constraints")));
            Forces.AddRange(Loader.LoadObjects<IOpenMMResidueTerm>(context, "Force", node.SelectSingleNode("Forces")));
        }

        private void LoadFromOpenMMXmlFile(string systemXmlFile, LoadContext context)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(Helper.ResolvePath(systemXmlFile));
            if (doc.DocumentElement != null)
            {
                Constraints.AddRange(Loader.LoadObjects<ConstraintResidueTerm>(context,
                    doc.DocumentElement.SelectSingleNode("Constraints")));
                Forces.AddRange(Loader.LoadObjects<IOpenMMResidueTerm>(context, "Force",
                    doc.DocumentElement.SelectSingleNode("Forces")));
            }
            else
            {
                throw new FileLoadException($"OpenMM System XML document does not appear to be valid. Unable to read Constraints or Forces from it.");
            }
        }


        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "TargetPlatform", desiredPlatform);
            Loader.SaveObjects(context, element, Constraints, "Constraints");
            Loader.SaveObjects(context, element, Forces, "Forces");
            return element;
        }

        public bool HasConstraints()
        {
            return Constraints.Count > 0;
        }
    }
}