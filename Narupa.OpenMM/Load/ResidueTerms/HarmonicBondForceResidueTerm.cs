﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Nano;
using Nano.Loading;
using Nano.Science.Simulation.Instantiated;
using Nano.Science.Simulation.Residues;
using Nano.Science.Simulation.Spawning;
using Narupa.OpenMM.Load.Terms;
using System.Collections.Generic;
using System.Xml;

namespace Narupa.OpenMM.Load.ResidueTerms
{
    [XmlName("HarmonicBondForce")]
    internal class HarmonicBondForceResidueTerm : IOpenMMResidueTerm
    {
        public int ForceGroup;
        public int Version;
        public readonly List<BondForceTermPath> Bonds = new List<BondForceTermPath>();

        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "forceGroup", ForceGroup);
            Helper.AppendAttributeAndValue(element, "version", Version);
            Loader.SaveObjects(context, element, Bonds, "Bonds");
            return element;
        }

        public void Load(LoadContext context, XmlNode node)
        {
            ForceGroup = Helper.GetAttributeValue(node, "forceGroup", ForceGroup);
            Version = Helper.GetAttributeValue(node, "version", Version);

            Bonds.AddRange(Loader.LoadObjects<BondForceTermPath>(context, node.SelectSingleNode("Bonds")));
        }

        public void AddToOpenMMTerms(OpenMMTerms openMMTerms, InstantiatedTopology parentTopology, IResidue residue, ref SpawnContext spawn)
        {
            HarmonicBondForceTerm bondForce = Utils.GetInstanceOf<HarmonicBondForceTerm>(openMMTerms.Forces);
            if (bondForce == null)
            {
                bondForce = new HarmonicBondForceTerm();
                openMMTerms.Forces.Add(bondForce);
            }
            foreach (BondForceTermPath bondPath in Bonds)
            {
                int atom1 = spawn.ResolveIndexOfAtom(parentTopology, bondPath.P1);
                int atom2 = spawn.ResolveIndexOfAtom(parentTopology, bondPath.P2);
                bondForce.Bonds.Add(new BondForceTerm(atom1, atom2, bondPath.D, bondPath.K));
            }
            bondForce.ForceGroup = ForceGroup;
        }
    }

    [XmlName("Bond")]
    internal class BondForceTermPath : ILoadable
    {
        public float D;
        public float K;
        public AtomPath P1;
        public AtomPath P2;

        public void Load(LoadContext context, XmlNode node)
        {
            D = Helper.GetAttributeValue(node, "d", D);
            P1 = new AtomPath(Helper.GetAttributeValue(node, "p1", string.Empty));
            P2 = new AtomPath(Helper.GetAttributeValue(node, "p2", string.Empty));
            K = Helper.GetAttributeValue(node, "k", K);
        }

        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "d", D);
            Helper.AppendAttributeAndValue(element, "p1", P1.ToString());
            Helper.AppendAttributeAndValue(element, "p2", P2.ToString());
            Helper.AppendAttributeAndValue(element, "k", K);
            return element;
        }
    }
}