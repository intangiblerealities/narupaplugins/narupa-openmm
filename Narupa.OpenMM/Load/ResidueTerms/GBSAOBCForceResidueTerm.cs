﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Nano.Loading;
using Nano.Science.Simulation.Instantiated;
using Nano.Science.Simulation.Residues;
using Nano.Science.Simulation.Spawning;
using Narupa.OpenMM.Load.Terms;
using System;
using System.Collections.Generic;
using System.Xml;
using Nano;

namespace Narupa.OpenMM.Load.ResidueTerms
{
    /// <summary>
    /// GBSA-OBC implicit solvent force.
    /// </summary>
    /// <seealso cref="Narupa.OpenMM.Load.ResidueTerms.IOpenMMResidueTerm" />
    [XmlName("GBSAOBCForce")]
    internal class GBSAOBCForceResidueTerm : IOpenMMResidueTerm
    {
        public GBSAOBCOptions Options = new GBSAOBCOptions();

        /// <summary>
        /// The particles in the GBSAOBC force.
        /// </summary>
        public List<GBSAOBCParticle> Particles = new List<GBSAOBCParticle>();

        /// <summary>
        /// Loads the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="node">The node.</param>
        public void Load(LoadContext context, XmlNode node)
        {
            Options.ForceGroup = Helper.GetAttributeValue(node, "forceGroup", Options.ForceGroup);
            Options.CutOff = Helper.GetAttributeValue(node, "cutoff", Options.CutOff);
            Options.Method = Helper.GetAttributeValue(node, "method", Options.Method);
            Options.SoluteDielectric = Helper.GetAttributeValue(node, "soluteDielectric", Options.SoluteDielectric);
            Options.SolventDielectric = Helper.GetAttributeValue(node, "solventDielectric", Options.SolventDielectric);
            Options.SurfaceAreaEnergy = Helper.GetAttributeValue(node, "surfaceAreaEnergy", Options.SurfaceAreaEnergy);
            Options.Version = Helper.GetAttributeValue(node, "version", Options.Version);
            Particles.AddRange(Loader.LoadObjects<GBSAOBCParticle>(context, node.SelectSingleNode("Particles")));
        }

        /// <summary>
        /// Saves the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The element.</param>
        /// <returns>XmlElement.</returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "forceGroup", Options.ForceGroup);
            Helper.AppendAttributeAndValue(element, "cutoff", Options.CutOff);
            Helper.AppendAttributeAndValue(element, "method", Options.Method);
            Helper.AppendAttributeAndValue(element, "soluteDielectric", Options.SoluteDielectric);
            Helper.AppendAttributeAndValue(element, "solventDielectric", Options.SolventDielectric);
            Helper.AppendAttributeAndValue(element, "surfaceAreaEnergy", Options.SurfaceAreaEnergy);
            Helper.AppendAttributeAndValue(element, "version", Options.Version);
            element.AppendChild(Loader.SaveObjects(context, element, Particles, "Particles"));
            return element;
        }

        public void AddToOpenMMTerms(OpenMMTerms openMMTerms, InstantiatedTopology parentTopology, IResidue residue, ref SpawnContext spawn)
        {
            GBSAOBCForceTerm GBSAOBCForce = Utils.GetInstanceOf<GBSAOBCForceTerm>(openMMTerms.Forces);
            if (GBSAOBCForce == null)
            {
                GBSAOBCForce = new GBSAOBCForceTerm();
                openMMTerms.Forces.Add(GBSAOBCForce);
            }
            GBSAOBCForce.Particles.AddRange(Particles);

            GBSAOBCForce.Options = Options;
        }
    }

    internal class GBSAOBCOptions
    {
        public float CutOff;
        public int ForceGroup;
        public int Method;
        public int SoluteDielectric;
        public float SolventDielectric;
        public float SurfaceAreaEnergy;
        public int Version;
    }

    [XmlName("Particle")]
    internal class GBSAOBCParticle : ILoadable
    {
        public float Scale;
        public float Radius;
        public float Charge;

        public void Load(LoadContext context, XmlNode node)
        {
            Scale = Helper.GetAttributeValue(node, "scale", Scale);
            Charge = Helper.GetAttributeValue(node, "q", Charge);
            Radius = Helper.GetAttributeValue(node, "r", Radius);
        }

        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "scale", Scale);
            Helper.AppendAttributeAndValue(element, "q", Charge);
            Helper.AppendAttributeAndValue(element, "r", Radius);
            return element;
        }
    }
}