﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Nano;
using Nano.Loading;
using Nano.Science.Simulation.Instantiated;
using Nano.Science.Simulation.Residues;
using Nano.Science.Simulation.Spawning;
using Narupa.OpenMM.Load.Terms;
using System.Collections.Generic;
using System.Xml;

namespace Narupa.OpenMM.Load.ResidueTerms
{
    [XmlName("PeriodicTorsionForce")]
    internal class PeriodicTorsionForceResidueTerm : IOpenMMResidueTerm
    {
        public int ForceGroup;
        public int Version;
        public readonly List<TorsionForceTermPath> Torsions = new List<TorsionForceTermPath>();

        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "forceGroup", ForceGroup);
            Helper.AppendAttributeAndValue(element, "version", Version);

            Loader.SaveObjects(context, element, Torsions, "Torsions");
            return element;
        }

        public void Load(LoadContext context, XmlNode node)
        {
            ForceGroup = Helper.GetAttributeValue(node, "forceGroup", ForceGroup);
            Version = Helper.GetAttributeValue(node, "version", Version);

            Torsions.AddRange(Loader.LoadObjects<TorsionForceTermPath>(context, node.SelectSingleNode("Torsions")));
        }

        public void AddToOpenMMTerms(OpenMMTerms openMMTerms, InstantiatedTopology parentTopology, IResidue residue, ref SpawnContext spawn)
        {
            PeriodicTorsionForceTerm torsionForce = Utils.GetInstanceOf<PeriodicTorsionForceTerm>(openMMTerms.Forces);
            if (torsionForce == null)
            {
                torsionForce = new PeriodicTorsionForceTerm();
                openMMTerms.Forces.Add(torsionForce);
            }
            foreach (TorsionForceTermPath torsion in Torsions)
            {
                int particle1 = spawn.ResolveIndexOfAtom(parentTopology, torsion.P1);
                int particle2 = spawn.ResolveIndexOfAtom(parentTopology, torsion.P2);
                int particle3 = spawn.ResolveIndexOfAtom(parentTopology, torsion.P3);
                int particle4 = spawn.ResolveIndexOfAtom(parentTopology, torsion.P4);
                torsionForce.Torsions.Add(new TorsionTerm(particle1, particle2, particle3, particle4, torsion.K, torsion.Periodicity, torsion.Phase));
            }
            torsionForce.ForceGroup = ForceGroup;
        }
    }

    [XmlName("Torsion")]
    internal class TorsionForceTermPath : ILoadable
    {
        public int Periodicity;
        public float Phase;
        public float K;
        public AtomPath P1;
        public AtomPath P2;
        public AtomPath P3;
        public AtomPath P4;

        public void Load(LoadContext context, XmlNode node)
        {
            Periodicity = Helper.GetAttributeValue(node, "periodicity", Periodicity);
            Phase = Helper.GetAttributeValue(node, "phase", Phase);
            P1 = new AtomPath(Helper.GetAttributeValue(node, "p1", string.Empty));
            P2 = new AtomPath(Helper.GetAttributeValue(node, "p2", string.Empty));
            P3 = new AtomPath(Helper.GetAttributeValue(node, "p3", string.Empty));
            P4 = new AtomPath(Helper.GetAttributeValue(node, "p4", string.Empty));
            K = Helper.GetAttributeValue(node, "k", K);
        }

        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "periodicity", Periodicity);
            Helper.AppendAttributeAndValue(element, "phase", Phase);
            Helper.AppendAttributeAndValue(element, "p1", P1.ToString());
            Helper.AppendAttributeAndValue(element, "p2", P2.ToString());
            Helper.AppendAttributeAndValue(element, "p3", P3.ToString());
            Helper.AppendAttributeAndValue(element, "p4", P4.ToString());
            Helper.AppendAttributeAndValue(element, "k", K);
            return element;
        }
    }
}