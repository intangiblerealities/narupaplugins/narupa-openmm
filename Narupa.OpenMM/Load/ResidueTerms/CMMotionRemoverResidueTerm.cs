﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Xml;
using Nano.Science.Simulation.Instantiated;
using Nano.Science.Simulation.Residues;
using Nano.Science.Simulation.Spawning;
using Narupa.OpenMM.Load.Terms;
using Nano.Loading;
using Nano;

namespace Narupa.OpenMM.Load.ResidueTerms
{
    [XmlName("CMMotionRemover")]
    internal class CMMotionRemoverResidueTerm : IOpenMMResidueTerm
    {
        public int ForceGroup;
        public int Frequency;
        public int Version;

        public void AddToOpenMMTerms(OpenMMTerms openMMTerms, InstantiatedTopology parentTopology, IResidue residue, ref SpawnContext spawn)
        {
            CMMotionRemoverTerm cmForce = Utils.GetInstanceOf<CMMotionRemoverTerm>(openMMTerms.Forces);
            if (cmForce == null)
            {
                cmForce = new CMMotionRemoverTerm();
                openMMTerms.Forces.Add(cmForce);
            }
            //override forcegroup with max of what this residue has.
            cmForce.ForceGroup = Math.Max(ForceGroup, cmForce.ForceGroup);
            //set frequency to be the minimum of all the residues.
            cmForce.Frequency = Math.Min(Frequency, cmForce.Frequency);
        }

        public void Load(LoadContext context, XmlNode node)
        {
            ForceGroup = Helper.GetAttributeValue(node, "forceGroup", ForceGroup);
            Frequency = Helper.GetAttributeValue(node, "frequency", Frequency);
            Version = Helper.GetAttributeValue(node, "version", Version);
        }

        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "forceGroup", ForceGroup);
            Helper.AppendAttributeAndValue(element, "frequency", Frequency);
            Helper.AppendAttributeAndValue(element, "version", Version);
            return element;
        }
    }
}