﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Nano.Science.Simulation.Instantiated;
using Nano.Science.Simulation.Residues;
using Nano.Science.Simulation.Spawning;
using Narupa.OpenMM.Load.Terms;
using System.Collections.Generic;
using System.Xml;
using Nano;
using Nano.Loading;

namespace Narupa.OpenMM.Load.ResidueTerms
{
    [XmlName("HarmonicAngleForce")]
    internal class HarmonicAngleForceResidueTerm : IOpenMMResidueTerm
    {
        public int ForceGroup;
        public int Version;
        public readonly List<AngleForceTermPath> Angles = new List<AngleForceTermPath>();

        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "forceGroup", ForceGroup);
            Helper.AppendAttributeAndValue(element, "version", Version);
            Loader.SaveObjects(context, element, Angles, "Angles");
            return element;
        }

        public void Load(LoadContext context, XmlNode node)
        {
            ForceGroup = Helper.GetAttributeValue(node, "forceGroup", ForceGroup);
            Version = Helper.GetAttributeValue(node, "version", Version);

            Angles.AddRange(Loader.LoadObjects<AngleForceTermPath>(context, node.SelectSingleNode("Angles")));
        }

        public void AddToOpenMMTerms(OpenMMTerms openMMTerms, InstantiatedTopology parentTopology, IResidue residue, ref SpawnContext spawn)
        {
            HarmonicAngleForceTerm angleForce = Utils.GetInstanceOf<HarmonicAngleForceTerm>(openMMTerms.Forces);
            if (angleForce == null)
            {
                angleForce = new HarmonicAngleForceTerm();
                openMMTerms.Forces.Add(angleForce);
            }
            foreach (AngleForceTermPath anglePath in Angles)
            {
                int atom1 = spawn.ResolveIndexOfAtom(parentTopology, anglePath.P1);
                int atom2 = spawn.ResolveIndexOfAtom(parentTopology, anglePath.P2);
                int atom3 = spawn.ResolveIndexOfAtom(parentTopology, anglePath.P3);
                angleForce.Angles.Add(new AngleForceTerm(atom1, atom2, atom3, anglePath.A, anglePath.K));
            }
            angleForce.ForceGroup = ForceGroup;
        }
    }

    [XmlName("Angle")]
    internal class AngleForceTermPath : ILoadable
    {
        public float A;
        public float K;
        public AtomPath P1;
        public AtomPath P2;
        public AtomPath P3;

        public void Load(LoadContext context, XmlNode node)
        {
            A = Helper.GetAttributeValue(node, "a", A);
            P1 = new AtomPath(Helper.GetAttributeValue(node, "p1", string.Empty));
            P2 = new AtomPath(Helper.GetAttributeValue(node, "p2", string.Empty));
            P3 = new AtomPath(Helper.GetAttributeValue(node, "p3", string.Empty));
            K = Helper.GetAttributeValue(node, "k", K);
        }

        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "a", A);
            Helper.AppendAttributeAndValue(element, "p1", P1.ToString());
            Helper.AppendAttributeAndValue(element, "p2", P2.ToString());
            Helper.AppendAttributeAndValue(element, "p3", P3.ToString());
            Helper.AppendAttributeAndValue(element, "k", K);
            return element;
        }
    }
}