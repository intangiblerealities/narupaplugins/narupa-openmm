﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Nano.Science.Simulation.Instantiated;
using Nano.Science.Simulation.Residues;
using Nano.Science.Simulation.Spawning;
using Narupa.OpenMM.Load.Terms;
using System;
using System.Collections.Generic;
using System.Xml;
using Nano;
using Nano.Loading;

namespace Narupa.OpenMM.Load.ResidueTerms
{
    [XmlName("NonbondedForce")]
    internal class NonBondedForceResidueTerm : IOpenMMResidueTerm
    {
        public NonBondedOptions Options = new NonBondedOptions();

        /// <summary>
        /// The particles in the nonbonded force.
        /// </summary>
        public List<NonBondedParticle> Particles = new List<NonBondedParticle>();

        /// <summary>
        /// The exceptions in the nonbonded force.
        /// </summary>
        public List<NonBondedExceptionPath> Exceptions = new List<NonBondedExceptionPath>();

        private bool generateExceptions;

        /// <summary>
        /// Loads the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="node">The node.</param>
        public void Load(LoadContext context, XmlNode node)
        {
            Options.Alpha = Helper.GetAttributeValue(node, "alpha", Options.Alpha);
            Options.CutOff = Helper.GetAttributeValue(node, "cutoff", Options.CutOff);
            Options.DispersionCorrection = Helper.GetAttributeValue(node, "dispersionCorrection", Options.DispersionCorrection);
            Options.EwaldTolerance = Helper.GetAttributeValue(node, "ewaldTolerance", Options.EwaldTolerance);
            Options.ForceGroup = Helper.GetAttributeValue(node, "forceGroup", Options.ForceGroup);
            Options.Method = Helper.GetAttributeValue(node, "method", Options.Method);
            Options.Nx = Helper.GetAttributeValue(node, "nx", Options.Nx);
            Options.Ny = Helper.GetAttributeValue(node, "ny", Options.Ny);
            Options.Nz = Helper.GetAttributeValue(node, "nz", Options.Nz);
            Options.RecipForceGroup = Helper.GetAttributeValue(node, "recipForceGroup", Options.RecipForceGroup);
            Options.RFDielectric = Helper.GetAttributeValue(node, "rfDielectric", Options.RFDielectric);
            Options.SwitchingDistance = Helper.GetAttributeValue(node, "switchingDistance", Options.SwitchingDistance);
            Options.UseSwitchingFunction = Helper.GetAttributeValue(node, "useSwitchingFunction", Options.UseSwitchingFunction);
            Options.Version = Helper.GetAttributeValue(node, "version", Options.Version);
            generateExceptions = Helper.GetAttributeValue(node, "GenerateExceptions", generateExceptions);

            Particles.AddRange(Loader.LoadObjects<NonBondedParticle>(context, node.SelectSingleNode("Particles")));
            Exceptions.AddRange(Loader.LoadObjects<NonBondedExceptionPath>(context, node.SelectSingleNode("Exceptions")));
        }

        /// <summary>
        /// Saves the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The element.</param>
        /// <returns>XmlElement.</returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "alpha", Options.Alpha);
            Helper.AppendAttributeAndValue(element, "cutoff", Options.CutOff);
            Helper.AppendAttributeAndValue(element, "dispersionCorrection", Options.DispersionCorrection);
            Helper.AppendAttributeAndValue(element, "ewaldTolerance", Options.EwaldTolerance);
            Helper.AppendAttributeAndValue(element, "forceGroup", Options.ForceGroup);
            Helper.AppendAttributeAndValue(element, "method", Options.Method);
            Helper.AppendAttributeAndValue(element, "nx", Options.Nx);
            Helper.AppendAttributeAndValue(element, "ny", Options.Ny);
            Helper.AppendAttributeAndValue(element, "nz", Options.Nz);
            Helper.AppendAttributeAndValue(element, "recipForceGroup", Options.RecipForceGroup);
            Helper.AppendAttributeAndValue(element, "rfDielectric", Options.RFDielectric);
            Helper.AppendAttributeAndValue(element, "switchingDistance", Options.SwitchingDistance);
            Helper.AppendAttributeAndValue(element, "useSwitchingFunction", Options.UseSwitchingFunction);
            Helper.AppendAttributeAndValue(element, "version", Options.Version);
            Helper.AppendAttributeAndValue(element, "GenerateExceptions", generateExceptions);
            element.AppendChild(Loader.SaveObjects(context, element, Particles, "Particles"));
            element.AppendChild(Loader.SaveObjects(context, element, Exceptions, "Exceptions"));
            return element;
        }

        public void AddToOpenMMTerms(OpenMMTerms openMMTerms, InstantiatedTopology parentTopology, IResidue residue, ref SpawnContext spawn)
        {
            NonBondedForceTerm nonBondedForce = Utils.GetInstanceOf<NonBondedForceTerm>(openMMTerms.Forces);
            if (nonBondedForce == null)
            {
                nonBondedForce = new NonBondedForceTerm();
                openMMTerms.Forces.Add(nonBondedForce);
            }
            nonBondedForce.Particles.AddRange(Particles);

            if (generateExceptions)
            {
                nonBondedForce.GenerateExceptions = true;
            }
            else
            {
                foreach (NonBondedExceptionPath exception in Exceptions)
                {
                    int atom1 = spawn.ResolveIndexOfAtom(parentTopology, exception.P1);
                    int atom2 = spawn.ResolveIndexOfAtom(parentTopology, exception.P2);

                    nonBondedForce.Exceptions.Add(new ExceptionTerm(atom1, atom2, exception.Epsilon, exception.Sigma, exception.Q));
                }
            }
            nonBondedForce.Options = Options;
        }
    }

    internal class NonBondedOptions
    {
        //TODO come up with good defaults for these.
        public float Alpha;

        public float CutOff;
        public bool DispersionCorrection;
        public float EwaldTolerance;
        public int ForceGroup;
        public int Method;
        public int Nx;
        public int Ny;
        public int Nz;
        public int RecipForceGroup;
        public float RFDielectric;
        public float SwitchingDistance;
        public bool UseSwitchingFunction;
        public int Version;
    }

    [XmlName("Particle")]
    internal class NonBondedParticle : ILoadable
    {
        public float Epsilon;
        public float Sigma;
        public float Charge;

        public void Load(LoadContext context, XmlNode node)
        {
            Epsilon = Helper.GetAttributeValue(node, "eps", Epsilon);
            Charge = Helper.GetAttributeValue(node, "q", Charge);
            Sigma = Helper.GetAttributeValue(node, "sig", Sigma);
        }

        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "eps", Epsilon);
            Helper.AppendAttributeAndValue(element, "q", Charge);
            Helper.AppendAttributeAndValue(element, "sig", Sigma);
            return element;
        }
    }

    [XmlName("Exception")]
    internal class NonBondedExceptionPath : ILoadable
    {
        public float Epsilon;
        public AtomPath P1;
        public AtomPath P2;
        public float Q;
        public float Sigma;

        public void Load(LoadContext context, XmlNode node)
        {
            Epsilon = Helper.GetAttributeValue(node, "eps", Epsilon);
            P1 = new AtomPath(Helper.GetAttributeValue(node, "p1", string.Empty));
            P2 = new AtomPath(Helper.GetAttributeValue(node, "p2", string.Empty));
            Q = Helper.GetAttributeValue(node, "q", Q);
            Sigma = Helper.GetAttributeValue(node, "sig", Sigma);
        }

        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "eps", Epsilon);
            Helper.AppendAttributeAndValue(element, "p1", P1.ToString());
            Helper.AppendAttributeAndValue(element, "p2", P2.ToString());
            Helper.AppendAttributeAndValue(element, "q", Q);
            Helper.AppendAttributeAndValue(element, "sig", Sigma);
            return element;
        }
    }
}