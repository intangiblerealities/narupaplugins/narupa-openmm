//------------------------------------------------------------------------------
// <auto-generated />
//
// This file was automatically generated by SWIG (http://www.swig.org).
// Version 3.0.12
//
// Do not make changes to this file unless you know what you are doing--modify
// the SWIG interface file instead.
//------------------------------------------------------------------------------

namespace OpenMMNET {

public class MonteCarloBarostat : Force {
  private global::System.Runtime.InteropServices.HandleRef swigCPtr;

  internal MonteCarloBarostat(global::System.IntPtr cPtr, bool cMemoryOwn) : base(OpenMMSwigPINVOKE.MonteCarloBarostat_SWIGUpcast(cPtr), cMemoryOwn) {
    swigCPtr = new global::System.Runtime.InteropServices.HandleRef(this, cPtr);
  }

  internal static global::System.Runtime.InteropServices.HandleRef getCPtr(MonteCarloBarostat obj) {
    return (obj == null) ? new global::System.Runtime.InteropServices.HandleRef(null, global::System.IntPtr.Zero) : obj.swigCPtr;
  }

  ~MonteCarloBarostat() {
    Dispose();
  }

  public override void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != global::System.IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          OpenMMSwigPINVOKE.delete_MonteCarloBarostat(swigCPtr);
        }
        swigCPtr = new global::System.Runtime.InteropServices.HandleRef(null, global::System.IntPtr.Zero);
      }
      global::System.GC.SuppressFinalize(this);
      base.Dispose();
    }
  }

  public static string Pressure() {
    string ret = OpenMMSwigPINVOKE.MonteCarloBarostat_Pressure();
    return ret;
  }

  public static string Temperature() {
    string ret = OpenMMSwigPINVOKE.MonteCarloBarostat_Temperature();
    return ret;
  }

  public MonteCarloBarostat(double defaultPressure, double defaultTemperature, int frequency) : this(OpenMMSwigPINVOKE.new_MonteCarloBarostat__SWIG_0(defaultPressure, defaultTemperature, frequency), true) {
  }

  public MonteCarloBarostat(double defaultPressure, double defaultTemperature) : this(OpenMMSwigPINVOKE.new_MonteCarloBarostat__SWIG_1(defaultPressure, defaultTemperature), true) {
  }

  public double getDefaultPressure() {
    double ret = OpenMMSwigPINVOKE.MonteCarloBarostat_getDefaultPressure(swigCPtr);
    return ret;
  }

  public void setDefaultPressure(double pressure) {
    OpenMMSwigPINVOKE.MonteCarloBarostat_setDefaultPressure(swigCPtr, pressure);
  }

  public int getFrequency() {
    int ret = OpenMMSwigPINVOKE.MonteCarloBarostat_getFrequency(swigCPtr);
    return ret;
  }

  public void setFrequency(int freq) {
    OpenMMSwigPINVOKE.MonteCarloBarostat_setFrequency(swigCPtr, freq);
  }

  public double getDefaultTemperature() {
    double ret = OpenMMSwigPINVOKE.MonteCarloBarostat_getDefaultTemperature(swigCPtr);
    return ret;
  }

  public void setDefaultTemperature(double temp) {
    OpenMMSwigPINVOKE.MonteCarloBarostat_setDefaultTemperature(swigCPtr, temp);
  }

  public int getRandomNumberSeed() {
    int ret = OpenMMSwigPINVOKE.MonteCarloBarostat_getRandomNumberSeed(swigCPtr);
    return ret;
  }

  public void setRandomNumberSeed(int seed) {
    OpenMMSwigPINVOKE.MonteCarloBarostat_setRandomNumberSeed(swigCPtr, seed);
  }

  public override bool usesPeriodicBoundaryConditions() {
    bool ret = OpenMMSwigPINVOKE.MonteCarloBarostat_usesPeriodicBoundaryConditions(swigCPtr);
    return ret;
  }

}

}
