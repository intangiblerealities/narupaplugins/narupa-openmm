//------------------------------------------------------------------------------
// <auto-generated />
//
// This file was automatically generated by SWIG (http://www.swig.org).
// Version 3.0.12
//
// Do not make changes to this file unless you know what you are doing--modify
// the SWIG interface file instead.
//------------------------------------------------------------------------------

namespace OpenMMNET {

public class TwoParticleAverageSite : VirtualSite {
  private global::System.Runtime.InteropServices.HandleRef swigCPtr;

  internal TwoParticleAverageSite(global::System.IntPtr cPtr, bool cMemoryOwn) : base(OpenMMSwigPINVOKE.TwoParticleAverageSite_SWIGUpcast(cPtr), cMemoryOwn) {
    swigCPtr = new global::System.Runtime.InteropServices.HandleRef(this, cPtr);
  }

  internal static global::System.Runtime.InteropServices.HandleRef getCPtr(TwoParticleAverageSite obj) {
    return (obj == null) ? new global::System.Runtime.InteropServices.HandleRef(null, global::System.IntPtr.Zero) : obj.swigCPtr;
  }

  ~TwoParticleAverageSite() {
    Dispose();
  }

  public override void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != global::System.IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          OpenMMSwigPINVOKE.delete_TwoParticleAverageSite(swigCPtr);
        }
        swigCPtr = new global::System.Runtime.InteropServices.HandleRef(null, global::System.IntPtr.Zero);
      }
      global::System.GC.SuppressFinalize(this);
      base.Dispose();
    }
  }

  public TwoParticleAverageSite(int particle1, int particle2, double weight1, double weight2) : this(OpenMMSwigPINVOKE.new_TwoParticleAverageSite(particle1, particle2, weight1, weight2), true) {
  }

  public double getWeight(int particle) {
    double ret = OpenMMSwigPINVOKE.TwoParticleAverageSite_getWeight(swigCPtr, particle);
    return ret;
  }

}

}
