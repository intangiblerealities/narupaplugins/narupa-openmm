//------------------------------------------------------------------------------
// <auto-generated />
//
// This file was automatically generated by SWIG (http://www.swig.org).
// Version 3.0.12
//
// Do not make changes to this file unless you know what you are doing--modify
// the SWIG interface file instead.
//------------------------------------------------------------------------------

namespace OpenMMNET {

public class CustomExternalForce : Force {
  private global::System.Runtime.InteropServices.HandleRef swigCPtr;

  internal CustomExternalForce(global::System.IntPtr cPtr, bool cMemoryOwn) : base(OpenMMSwigPINVOKE.CustomExternalForce_SWIGUpcast(cPtr), cMemoryOwn) {
    swigCPtr = new global::System.Runtime.InteropServices.HandleRef(this, cPtr);
  }

  internal static global::System.Runtime.InteropServices.HandleRef getCPtr(CustomExternalForce obj) {
    return (obj == null) ? new global::System.Runtime.InteropServices.HandleRef(null, global::System.IntPtr.Zero) : obj.swigCPtr;
  }

  ~CustomExternalForce() {
    Dispose();
  }

  public override void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != global::System.IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          OpenMMSwigPINVOKE.delete_CustomExternalForce(swigCPtr);
        }
        swigCPtr = new global::System.Runtime.InteropServices.HandleRef(null, global::System.IntPtr.Zero);
      }
      global::System.GC.SuppressFinalize(this);
      base.Dispose();
    }
  }

  public CustomExternalForce(string energy) : this(OpenMMSwigPINVOKE.new_CustomExternalForce(energy), true) {
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
  }

  public int getNumParticles() {
    int ret = OpenMMSwigPINVOKE.CustomExternalForce_getNumParticles(swigCPtr);
    return ret;
  }

  public int getNumPerParticleParameters() {
    int ret = OpenMMSwigPINVOKE.CustomExternalForce_getNumPerParticleParameters(swigCPtr);
    return ret;
  }

  public int getNumGlobalParameters() {
    int ret = OpenMMSwigPINVOKE.CustomExternalForce_getNumGlobalParameters(swigCPtr);
    return ret;
  }

  public string getEnergyFunction() {
    string ret = OpenMMSwigPINVOKE.CustomExternalForce_getEnergyFunction(swigCPtr);
    return ret;
  }

  public void setEnergyFunction(string energy) {
    OpenMMSwigPINVOKE.CustomExternalForce_setEnergyFunction(swigCPtr, energy);
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
  }

  public int addPerParticleParameter(string name) {
    int ret = OpenMMSwigPINVOKE.CustomExternalForce_addPerParticleParameter(swigCPtr, name);
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public string getPerParticleParameterName(int index) {
    string ret = OpenMMSwigPINVOKE.CustomExternalForce_getPerParticleParameterName(swigCPtr, index);
    return ret;
  }

  public void setPerParticleParameterName(int index, string name) {
    OpenMMSwigPINVOKE.CustomExternalForce_setPerParticleParameterName(swigCPtr, index, name);
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
  }

  public int addGlobalParameter(string name, double defaultValue) {
    int ret = OpenMMSwigPINVOKE.CustomExternalForce_addGlobalParameter(swigCPtr, name, defaultValue);
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public string getGlobalParameterName(int index) {
    string ret = OpenMMSwigPINVOKE.CustomExternalForce_getGlobalParameterName(swigCPtr, index);
    return ret;
  }

  public void setGlobalParameterName(int index, string name) {
    OpenMMSwigPINVOKE.CustomExternalForce_setGlobalParameterName(swigCPtr, index, name);
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
  }

  public double getGlobalParameterDefaultValue(int index) {
    double ret = OpenMMSwigPINVOKE.CustomExternalForce_getGlobalParameterDefaultValue(swigCPtr, index);
    return ret;
  }

  public void setGlobalParameterDefaultValue(int index, double defaultValue) {
    OpenMMSwigPINVOKE.CustomExternalForce_setGlobalParameterDefaultValue(swigCPtr, index, defaultValue);
  }

  public int addParticle(int particle, SWIGTYPE_p_std__vectorT_double_t parameters) {
    int ret = OpenMMSwigPINVOKE.CustomExternalForce_addParticle__SWIG_0(swigCPtr, particle, SWIGTYPE_p_std__vectorT_double_t.getCPtr(parameters));
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public int addParticle(int particle) {
    int ret = OpenMMSwigPINVOKE.CustomExternalForce_addParticle__SWIG_1(swigCPtr, particle);
    return ret;
  }

  public void getParticleParameters(int index, SWIGTYPE_p_int particle, SWIGTYPE_p_std__vectorT_double_t parameters) {
    OpenMMSwigPINVOKE.CustomExternalForce_getParticleParameters(swigCPtr, index, SWIGTYPE_p_int.getCPtr(particle), SWIGTYPE_p_std__vectorT_double_t.getCPtr(parameters));
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
  }

  public void setParticleParameters(int index, int particle, SWIGTYPE_p_std__vectorT_double_t parameters) {
    OpenMMSwigPINVOKE.CustomExternalForce_setParticleParameters__SWIG_0(swigCPtr, index, particle, SWIGTYPE_p_std__vectorT_double_t.getCPtr(parameters));
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
  }

  public void setParticleParameters(int index, int particle) {
    OpenMMSwigPINVOKE.CustomExternalForce_setParticleParameters__SWIG_1(swigCPtr, index, particle);
  }

  public void updateParametersInContext(Context context) {
    OpenMMSwigPINVOKE.CustomExternalForce_updateParametersInContext(swigCPtr, Context.getCPtr(context));
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
  }

  public override bool usesPeriodicBoundaryConditions() {
    bool ret = OpenMMSwigPINVOKE.CustomExternalForce_usesPeriodicBoundaryConditions(swigCPtr);
    return ret;
  }

}

}
