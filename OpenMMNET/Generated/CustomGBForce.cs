//------------------------------------------------------------------------------
// <auto-generated />
//
// This file was automatically generated by SWIG (http://www.swig.org).
// Version 3.0.12
//
// Do not make changes to this file unless you know what you are doing--modify
// the SWIG interface file instead.
//------------------------------------------------------------------------------

namespace OpenMMNET {

public class CustomGBForce : Force {
  private global::System.Runtime.InteropServices.HandleRef swigCPtr;

  internal CustomGBForce(global::System.IntPtr cPtr, bool cMemoryOwn) : base(OpenMMSwigPINVOKE.CustomGBForce_SWIGUpcast(cPtr), cMemoryOwn) {
    swigCPtr = new global::System.Runtime.InteropServices.HandleRef(this, cPtr);
  }

  internal static global::System.Runtime.InteropServices.HandleRef getCPtr(CustomGBForce obj) {
    return (obj == null) ? new global::System.Runtime.InteropServices.HandleRef(null, global::System.IntPtr.Zero) : obj.swigCPtr;
  }

  ~CustomGBForce() {
    Dispose();
  }

  public override void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != global::System.IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          OpenMMSwigPINVOKE.delete_CustomGBForce(swigCPtr);
        }
        swigCPtr = new global::System.Runtime.InteropServices.HandleRef(null, global::System.IntPtr.Zero);
      }
      global::System.GC.SuppressFinalize(this);
      base.Dispose();
    }
  }

  public CustomGBForce() : this(OpenMMSwigPINVOKE.new_CustomGBForce(), true) {
  }

  public int getNumParticles() {
    int ret = OpenMMSwigPINVOKE.CustomGBForce_getNumParticles(swigCPtr);
    return ret;
  }

  public int getNumExclusions() {
    int ret = OpenMMSwigPINVOKE.CustomGBForce_getNumExclusions(swigCPtr);
    return ret;
  }

  public int getNumPerParticleParameters() {
    int ret = OpenMMSwigPINVOKE.CustomGBForce_getNumPerParticleParameters(swigCPtr);
    return ret;
  }

  public int getNumGlobalParameters() {
    int ret = OpenMMSwigPINVOKE.CustomGBForce_getNumGlobalParameters(swigCPtr);
    return ret;
  }

  public int getNumEnergyParameterDerivatives() {
    int ret = OpenMMSwigPINVOKE.CustomGBForce_getNumEnergyParameterDerivatives(swigCPtr);
    return ret;
  }

  public int getNumTabulatedFunctions() {
    int ret = OpenMMSwigPINVOKE.CustomGBForce_getNumTabulatedFunctions(swigCPtr);
    return ret;
  }

  public int getNumFunctions() {
    int ret = OpenMMSwigPINVOKE.CustomGBForce_getNumFunctions(swigCPtr);
    return ret;
  }

  public int getNumComputedValues() {
    int ret = OpenMMSwigPINVOKE.CustomGBForce_getNumComputedValues(swigCPtr);
    return ret;
  }

  public int getNumEnergyTerms() {
    int ret = OpenMMSwigPINVOKE.CustomGBForce_getNumEnergyTerms(swigCPtr);
    return ret;
  }

  public CustomGBForce.NonbondedMethod getNonbondedMethod() {
    CustomGBForce.NonbondedMethod ret = (CustomGBForce.NonbondedMethod)OpenMMSwigPINVOKE.CustomGBForce_getNonbondedMethod(swigCPtr);
    return ret;
  }

  public void setNonbondedMethod(CustomGBForce.NonbondedMethod method) {
    OpenMMSwigPINVOKE.CustomGBForce_setNonbondedMethod(swigCPtr, (int)method);
  }

  public double getCutoffDistance() {
    double ret = OpenMMSwigPINVOKE.CustomGBForce_getCutoffDistance(swigCPtr);
    return ret;
  }

  public void setCutoffDistance(double distance) {
    OpenMMSwigPINVOKE.CustomGBForce_setCutoffDistance(swigCPtr, distance);
  }

  public int addPerParticleParameter(string name) {
    int ret = OpenMMSwigPINVOKE.CustomGBForce_addPerParticleParameter(swigCPtr, name);
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public string getPerParticleParameterName(int index) {
    string ret = OpenMMSwigPINVOKE.CustomGBForce_getPerParticleParameterName(swigCPtr, index);
    return ret;
  }

  public void setPerParticleParameterName(int index, string name) {
    OpenMMSwigPINVOKE.CustomGBForce_setPerParticleParameterName(swigCPtr, index, name);
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
  }

  public int addGlobalParameter(string name, double defaultValue) {
    int ret = OpenMMSwigPINVOKE.CustomGBForce_addGlobalParameter(swigCPtr, name, defaultValue);
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public string getGlobalParameterName(int index) {
    string ret = OpenMMSwigPINVOKE.CustomGBForce_getGlobalParameterName(swigCPtr, index);
    return ret;
  }

  public void setGlobalParameterName(int index, string name) {
    OpenMMSwigPINVOKE.CustomGBForce_setGlobalParameterName(swigCPtr, index, name);
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
  }

  public double getGlobalParameterDefaultValue(int index) {
    double ret = OpenMMSwigPINVOKE.CustomGBForce_getGlobalParameterDefaultValue(swigCPtr, index);
    return ret;
  }

  public void setGlobalParameterDefaultValue(int index, double defaultValue) {
    OpenMMSwigPINVOKE.CustomGBForce_setGlobalParameterDefaultValue(swigCPtr, index, defaultValue);
  }

  public void addEnergyParameterDerivative(string name) {
    OpenMMSwigPINVOKE.CustomGBForce_addEnergyParameterDerivative(swigCPtr, name);
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
  }

  public string getEnergyParameterDerivativeName(int index) {
    string ret = OpenMMSwigPINVOKE.CustomGBForce_getEnergyParameterDerivativeName(swigCPtr, index);
    return ret;
  }

  public int addParticle(SWIGTYPE_p_std__vectorT_double_t parameters) {
    int ret = OpenMMSwigPINVOKE.CustomGBForce_addParticle__SWIG_0(swigCPtr, SWIGTYPE_p_std__vectorT_double_t.getCPtr(parameters));
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public int addParticle() {
    int ret = OpenMMSwigPINVOKE.CustomGBForce_addParticle__SWIG_1(swigCPtr);
    return ret;
  }

  public void getParticleParameters(int index, SWIGTYPE_p_std__vectorT_double_t parameters) {
    OpenMMSwigPINVOKE.CustomGBForce_getParticleParameters(swigCPtr, index, SWIGTYPE_p_std__vectorT_double_t.getCPtr(parameters));
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
  }

  public void setParticleParameters(int index, SWIGTYPE_p_std__vectorT_double_t parameters) {
    OpenMMSwigPINVOKE.CustomGBForce_setParticleParameters(swigCPtr, index, SWIGTYPE_p_std__vectorT_double_t.getCPtr(parameters));
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
  }

  public int addComputedValue(string name, string expression, CustomGBForce.ComputationType type) {
    int ret = OpenMMSwigPINVOKE.CustomGBForce_addComputedValue(swigCPtr, name, expression, (int)type);
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public void getComputedValueParameters(int index, SWIGTYPE_p_std__string name, SWIGTYPE_p_std__string expression, SWIGTYPE_p_OpenMM__CustomGBForce__ComputationType type) {
    OpenMMSwigPINVOKE.CustomGBForce_getComputedValueParameters(swigCPtr, index, SWIGTYPE_p_std__string.getCPtr(name), SWIGTYPE_p_std__string.getCPtr(expression), SWIGTYPE_p_OpenMM__CustomGBForce__ComputationType.getCPtr(type));
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
  }

  public void setComputedValueParameters(int index, string name, string expression, CustomGBForce.ComputationType type) {
    OpenMMSwigPINVOKE.CustomGBForce_setComputedValueParameters(swigCPtr, index, name, expression, (int)type);
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
  }

  public int addEnergyTerm(string expression, CustomGBForce.ComputationType type) {
    int ret = OpenMMSwigPINVOKE.CustomGBForce_addEnergyTerm(swigCPtr, expression, (int)type);
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public void getEnergyTermParameters(int index, SWIGTYPE_p_std__string expression, SWIGTYPE_p_OpenMM__CustomGBForce__ComputationType type) {
    OpenMMSwigPINVOKE.CustomGBForce_getEnergyTermParameters(swigCPtr, index, SWIGTYPE_p_std__string.getCPtr(expression), SWIGTYPE_p_OpenMM__CustomGBForce__ComputationType.getCPtr(type));
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
  }

  public void setEnergyTermParameters(int index, string expression, CustomGBForce.ComputationType type) {
    OpenMMSwigPINVOKE.CustomGBForce_setEnergyTermParameters(swigCPtr, index, expression, (int)type);
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
  }

  public int addExclusion(int particle1, int particle2) {
    int ret = OpenMMSwigPINVOKE.CustomGBForce_addExclusion(swigCPtr, particle1, particle2);
    return ret;
  }

  public void getExclusionParticles(int index, SWIGTYPE_p_int particle1, SWIGTYPE_p_int particle2) {
    OpenMMSwigPINVOKE.CustomGBForce_getExclusionParticles(swigCPtr, index, SWIGTYPE_p_int.getCPtr(particle1), SWIGTYPE_p_int.getCPtr(particle2));
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
  }

  public void setExclusionParticles(int index, int particle1, int particle2) {
    OpenMMSwigPINVOKE.CustomGBForce_setExclusionParticles(swigCPtr, index, particle1, particle2);
  }

  public int addTabulatedFunction(string name, TabulatedFunction function) {
    int ret = OpenMMSwigPINVOKE.CustomGBForce_addTabulatedFunction(swigCPtr, name, TabulatedFunction.getCPtr(function));
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public TabulatedFunction getTabulatedFunction(int index) {
    TabulatedFunction ret = new TabulatedFunction(OpenMMSwigPINVOKE.CustomGBForce_getTabulatedFunction__SWIG_0(swigCPtr, index), false);
    return ret;
  }

  public string getTabulatedFunctionName(int index) {
    string ret = OpenMMSwigPINVOKE.CustomGBForce_getTabulatedFunctionName(swigCPtr, index);
    return ret;
  }

  public int addFunction(string name, SWIGTYPE_p_std__vectorT_double_t values, double min, double max) {
    int ret = OpenMMSwigPINVOKE.CustomGBForce_addFunction(swigCPtr, name, SWIGTYPE_p_std__vectorT_double_t.getCPtr(values), min, max);
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public void getFunctionParameters(int index, SWIGTYPE_p_std__string name, SWIGTYPE_p_std__vectorT_double_t values, SWIGTYPE_p_double min, SWIGTYPE_p_double max) {
    OpenMMSwigPINVOKE.CustomGBForce_getFunctionParameters(swigCPtr, index, SWIGTYPE_p_std__string.getCPtr(name), SWIGTYPE_p_std__vectorT_double_t.getCPtr(values), SWIGTYPE_p_double.getCPtr(min), SWIGTYPE_p_double.getCPtr(max));
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
  }

  public void setFunctionParameters(int index, string name, SWIGTYPE_p_std__vectorT_double_t values, double min, double max) {
    OpenMMSwigPINVOKE.CustomGBForce_setFunctionParameters(swigCPtr, index, name, SWIGTYPE_p_std__vectorT_double_t.getCPtr(values), min, max);
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
  }

  public void updateParametersInContext(Context context) {
    OpenMMSwigPINVOKE.CustomGBForce_updateParametersInContext(swigCPtr, Context.getCPtr(context));
    if (OpenMMSwigPINVOKE.SWIGPendingException.Pending) throw OpenMMSwigPINVOKE.SWIGPendingException.Retrieve();
  }

  public override bool usesPeriodicBoundaryConditions() {
    bool ret = OpenMMSwigPINVOKE.CustomGBForce_usesPeriodicBoundaryConditions(swigCPtr);
    return ret;
  }

  public enum NonbondedMethod {
    NoCutoff = 0,
    CutoffNonPeriodic = 1,
    CutoffPeriodic = 2
  }

  public enum ComputationType {
    SingleParticle = 0,
    ParticlePair = 1,
    ParticlePairNoExclusions = 2
  }

}

}
