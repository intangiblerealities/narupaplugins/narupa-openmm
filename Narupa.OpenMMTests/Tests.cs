﻿using System;
using System.Linq;
using NUnit.Framework;
using NUnit.Framework.Constraints;
using Narupa.MD;
using Narupa.MD.System;
using SlimMath;

namespace Narupa.OpenMMTests
{
    [TestFixture]
    public class Tests
    {
        private TestHelper helper;

       
        [SetUp]
        public void Initialize()
        {
            //Test helper sorts out loading the plugin for us.
            helper = new TestHelper();
        }
        
        [Test]
        public void TestPotentialEnergy()
        {
            string simFile = "^/Assets/17-ALA.xml";
            Simulation sim = new Simulation(simFile, helper.Reporter);
            float energy = sim.ActiveSystem.CalculateForceFields();
            //Check that the energy is as expected.
            Assert.AreEqual(224.3367f, energy, 0.01f);
            sim.Dispose();
        }
        
        [Test]
        public void TestConstraints()
        {
            string simFile = "^/Assets/2efv_constraints.xml";
            Simulation sim = new Simulation(simFile, helper.Reporter);
            float energy = sim.ActiveSystem.CalculateForceFields();

            var constraints = sim.ActiveSystem.Constraints[0].GetConstraints();
            
            sim.Run(1000);
            
            //Check that the constraints are satisfied. 
            double tolerance = 0.0001;
            Assert.AreEqual(734, sim.ActiveSystem.Constraints[0].GetNumberOfConstraints());
            foreach (var constraint in constraints)
            {
                var dist = Vector3.Distance(sim.ActiveSystem.Particles.Position[constraint.A],
                    sim.ActiveSystem.Particles.Position[constraint.B]);

                var diff = Math.Abs(dist - constraint.Distance);
                Assert.AreEqual(diff, 0.0, tolerance);
            }
            sim.Dispose();
        }
        
        [Test]
        public void TestSettle()
        {
            string simFile = "^/Assets/TIP3P.xml";
            Simulation sim = new Simulation(simFile, helper.Reporter);
            float energy = sim.ActiveSystem.CalculateForceFields();

            var constraints = sim.ActiveSystem.Constraints[0].GetConstraints();
            
            sim.Run(1000);
            
            //Check that the constraints are satisfied. 
            double tolerance = 0.0001;
            Assert.AreEqual(9, sim.ActiveSystem.Constraints[0].GetNumberOfConstraints());
            foreach (var constraint in constraints)
            {
                var dist = Vector3.Distance(sim.ActiveSystem.Particles.Position[constraint.A],
                    sim.ActiveSystem.Particles.Position[constraint.B]);

                var diff = Math.Abs(dist - constraint.Distance);
                Assert.AreEqual(diff, 0.0, tolerance);
            }
            sim.Dispose();
        }
        
    }
}