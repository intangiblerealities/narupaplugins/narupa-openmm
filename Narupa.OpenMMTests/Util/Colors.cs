﻿using System;

namespace TestHelper.Util
{
    static class Colors
    {
        public const ConsoleColor Heading = ConsoleColor.White;
        public const ConsoleColor Emphasized = ConsoleColor.Cyan;
        public const ConsoleColor UserInput = ConsoleColor.DarkCyan;
        public const ConsoleColor Ident = ConsoleColor.DarkGray;
        public const ConsoleColor Normal = ConsoleColor.White;
        public const ConsoleColor Success = ConsoleColor.Green;
        public const ConsoleColor Error = ConsoleColor.Red;

        public const ConsoleColor ErrorDetail = ConsoleColor.DarkRed;
        public const ConsoleColor Transmit = ConsoleColor.DarkGreen;
        public const ConsoleColor Receive = ConsoleColor.DarkRed;
        public const ConsoleColor Message = ConsoleColor.Gray;

        public const ConsoleColor Action = ConsoleColor.DarkMagenta;
        public const ConsoleColor Debug = ConsoleColor.Blue;
        public const ConsoleColor Warning = ConsoleColor.Yellow;
    }
}
