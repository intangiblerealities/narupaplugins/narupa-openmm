copy_build () {
  mkdir -p ../../narupa/bin/$1/Plugins/OpenMMForceField/Managed/
  cp "Narupa.OpenMM/bin/Release/OpenMMNET.dll" ../../narupa/bin/$1/Plugins/OpenMMForceField/Managed/
  cp "Narupa.OpenMM/bin/Release/Narupa.OpenMM.dll" ../../narupa/bin/$1/Plugins/OpenMMForceField/Managed/
  cp "Narupa.OpenMM/PluginManifest.xml" ../../narupa/bin/$1/Plugins/OpenMMForceField/
}

copy_build debug
copy_build release

cp OpenMMWrapper/build/libOpenMMSwig.so /usr/lib && echo "\nOpenMM Narupa plugin installed. Ensure to set the OPENMM_PLUGIN_DIR variable in order to use the CPU, CUDA or OpenCL platforms. For an anaconda installation, this is something like [path/to/anaconda/]/lib/plugins"
